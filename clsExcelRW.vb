﻿'------------------------------------------------------------------------
'   clsExcelRW
'   Copyright(c)  2008 Yokogawa Electric Corporation.
'
'   Comment:  ExcelFile Access用 Class Module
'   HowToUse: Microsoft Excel xx.x ObjectLibrary 참조설정
'   Modify 
'   > Ver 0.00 | 2008/10/13 | New Release                               | K.Minami
'   > Ver 
'
'------------------------------------------------------------------------

Option Strict On
Option Explicit On 
Imports SRI = System.Runtime.InteropServices.Marshal
Imports VB = Microsoft.VisualBasic

Public Class clsExcelRW
    '----------------------------------------
    '   Const Define
    '----------------------------------------
    Private Const MaxBookNumber As Integer = 10         'Open가능한 최대WorkBook数

    '----------------------------------------
    '   Local Variable
    '----------------------------------------
    Private myXls As Excel.Application                  'Excel Object
    Private myXlsBooks As Excel.Workbooks               'Ecxel中 Book Collection
    Private myXlsBook(MaxBookNumber) As Excel.Workbook  'WorkBook
    Private myXlsSheets As Excel.Sheets                 'Book中 SheetCollection
    Private myXlsSheet As Excel.Worksheet               'WorkSheet

    '----------------------------------------
    '   Constructor
    '----------------------------------------
    Public Sub New()
        'Excel Application을 기동.
        myXls = CType(VB.CreateObject("Excel.Application"), Excel.Application)
    End Sub

    '----------------------------------------
    '   Property
    '----------------------------------------
    '선택되어진 WorkBook Name
    Public ReadOnly Property WorkBookName(Optional ByVal BookNumber As Integer = 0) As String
        Get
            If VB.IsNothing(myXlsBook(BookNumber)) Then
                WorkBookName = Nothing
            Else
                WorkBookName = myXlsBook(BookNumber).Name
            End If
        End Get
    End Property

    '선택되어진 Sheet Name
    Public ReadOnly Property WorkSheetName() As String
        Get
            If VB.IsNothing(myXlsSheet) Then
                WorkSheetName = Nothing
            Else
                WorkSheetName = myXlsSheet.Name
            End If
        End Get
    End Property

    '----------------------------------------
    '   Method
    '----------------------------------------
    'Excel 표시
    Public Sub ViewExcel()
        myXls.Application.Visible = True
        System.Windows.Forms.Application.DoEvents()
    End Sub

    'Excel 종료
    Public Sub QuitExcel()
        '    If VB.IsNothing(myXlsBooks) = False Then               'Book Collection Object 개방
        '   myXlsBooks.Close()
        '   SRI.ReleaseComObject(myXlsBooks)
        '  End If

        '  If VB.IsNothing(myXls) = False Then                    'Excel  Object 개방
        '  myXls.Quit()
        '  SRI.ReleaseComObject(myXls)
        '  End If

        Try

            If IsNothing(myXls) Then
                Return
            End If

            'Method #1: Gracefully exit app
            Try
                myXls.Visible = False
                myXls.ScreenUpdating = False
                If myXls.ActiveWorkbook Is Nothing Then Return
                myXls.ActiveWorkbook.Saved = True
                myXls.Quit()
                myXls = Nothing
            Catch
            End Try

            'Method #2: Nuke the temp file from orbit; kill EXCEL process
            If xlsProcID() <> 0 Then
                Dim proc As Process = Process.GetProcessById(xlProcID)
                proc.Kill()
            End If

            'Method #3: Physically find and kill the temp file.
            Dim fileExists As Boolean
            fileExists = My.Computer.FileSystem.FileExists(_TempFileLoc)

            If fileExists = True Then
                Try                             'Remove temp file
                    My.Computer.FileSystem.DeleteFile(_TempFileLoc, FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently)
                Catch
                End Try
            End If

        Catch

        Finally
            ReDim myXlsBook(0)                 'Destroy the array
            myXlsSheet = Nothing                'Release
            myXls = Nothing                     'Release
        End Try

    End Sub

    Private Function _TempFileLoc() As String
        Throw New NotImplementedException()
    End Function

    Private Function xlProcID() As Integer
        Throw New NotImplementedException()
    End Function

    Private Function xlsProcID() As Integer
        Throw New NotImplementedException()
    End Function

    'WorkBook(***.xls) OPEN. 없으면 신규작성
    Public Sub OpenWorkBook(ByVal BookName As String, Optional ByVal BookNumber As Integer = 0)
        If System.IO.File.Exists(BookName) = True Then
            myXlsBooks = CType(myXls.Workbooks, Excel.Workbooks)
            myXlsBook(BookNumber) = CType(myXls.Workbooks.Open(BookName), Excel.Workbook)
        Else
            myXlsBooks = CType(myXls.Workbooks, Excel.Workbooks)
            myXlsBook(BookNumber) = CType(myXls.Workbooks.Add, Excel.Workbook)
            myXlsBook(BookNumber).SaveAs(BookName)
        End If
    End Sub

    'WorkBook 보존
    Public Sub SaveWorkBook(Optional ByVal BookNumber As Integer = 0)

    End Sub

    'WorkBook Close
    Public Sub CloseWorkBook(Optional ByVal BookNumber As Integer = 0)

        myXls.DisplayAlerts = False

        If myXlsBook(BookNumber).Saved = False Then         'SAVE되지 않은 경우 강제종료!
            myXlsBook(BookNumber).Saved = True
        End If

        If VB.IsNothing(myXlsSheet) = False Then               'Sheet Object 개방
            SRI.ReleaseComObject(myXlsSheet)
        End If

        If VB.IsNothing(myXlsSheets) = False Then              'Sheet Collection Object 개방
            SRI.ReleaseComObject(myXlsSheets)
        End If

        If VB.IsNothing(myXlsBook(BookNumber)) = False Then    '지정된 WorkBook Object 개방
            myXlsBook(BookNumber).Close(False)
            SRI.ReleaseComObject(myXlsBook(BookNumber))
        End If
    End Sub

    'Sheet 선택(여기서 WorkBook지정도 실시)
    Public Function SelectSheet(ByVal SheetName As String, Optional ByVal BookNumber As Integer = 0) As Boolean
        SelectSheet = False
        If VB.IsNothing(myXlsBook(BookNumber)) = False Then    'Book이 존재한다면..
            myXlsSheets = CType(myXlsBook(BookNumber).Worksheets, Excel.Sheets)
            If Me.ExistSheet(SheetName) = True Then         'Sheet가 존재한다면..
                myXlsSheet = CType(myXlsBook(BookNumber).Worksheets(SheetName), Excel.Worksheet)
                SelectSheet = True
            End If
        End If
    End Function

    'Sheet 신규추가
    Public Function AddSheet(ByVal SheetName As String, Optional ByVal BookNumber As Integer = 0) As Boolean
        AddSheet = False
        If VB.IsNothing(myXlsBook(BookNumber)) = False Then    'Book이 존재한다면..
            myXlsSheets = CType(myXlsBook(BookNumber).Worksheets, Excel.Sheets)
            If Me.ExistSheet(SheetName) = False Then        'Sheet가 존재하지 않는다면..[추가&선택&Rename]
                myXlsSheets.Add()
                myXlsSheet = CType(myXlsBook(BookNumber).ActiveSheet, Excel.Worksheet)
                myXlsSheet.Name = SheetName
                AddSheet = True
            End If
        End If
    End Function

    'Sheet(선택(SelectSheet)되어져 있음) 출력(인쇄)
    Public Sub PrintSheet(Optional ByVal BookNumber As Integer = 0)
        If VB.IsNothing(myXlsBook(BookNumber)) Then Exit Sub
        myXlsSheet.PrintOut()
        Me.WaitTime(2000)

        ' frmNamePlate.txtMsCode.Text = ""
        ' frmNamePlate.txtSerialNo.Text = ""

        'frmNamePlate.txtMsCode.Focus()
    End Sub

    '지정한 Cell에 Data Write（행、열번호로 Cell 지정）
    Public Sub SetDataToCell(ByVal intRow As Integer, ByVal intCol As Integer, ByVal strData As String)
        If VB.IsNothing(myXlsSheet) Then Exit Sub
        myXlsSheet.Cells(intRow, intCol) = strData
    End Sub

    '지정한 Cell로 부터 Data Read（행、열번호로 Cell 지정）
    Public Function GetDataFromCell(ByVal intRow As Integer, ByVal intCol As Integer) As String
        If VB.IsNothing(myXlsSheet) Then Return ""
        'GetDataFromCell = myXlsSheet.Cells(intRow, intCol).Value
        'GetDataFromCell = CType(myXlsSheet.Cells(intRow, intCol), Excel.Range).Value.ToString
        GetDataFromCell = CStr(CType(myXlsSheet.Cells(intRow, intCol), Excel.Range).Value)
    End Function

    '----------------------------------------
    '   Private Function
    '----------------------------------------
    Private Function ExistSheet(ByVal SheetName As String) As Boolean
        Dim i As Integer
        Dim cnt As Integer

        cnt = myXlsSheets.Count
        ExistSheet = False
        For i = 1 To cnt
            If CType(myXlsSheets(i), Excel.Worksheet).Name = SheetName Then
                ExistSheet = True
                Exit For
            End If
        Next
    End Function

    Private Sub WaitTime(ByVal msec As Integer)
        System.Threading.Thread.Sleep(msec)     'mSec(秒) 대기
    End Sub

End Class
