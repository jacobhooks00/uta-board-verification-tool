﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtAssy = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtS1 = New System.Windows.Forms.TextBox()
        Me.txtE4 = New System.Windows.Forms.TextBox()
        Me.txtE3 = New System.Windows.Forms.TextBox()
        Me.txtE1 = New System.Windows.Forms.TextBox()
        Me.txtE2 = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtPartNum = New System.Windows.Forms.TextBox()
        Me.txtBaseMod = New System.Windows.Forms.TextBox()
        Me.cmdSubmit = New System.Windows.Forms.Button()
        Me.cmdClear = New System.Windows.Forms.Button()
        Me.cmdExit = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtMSG = New System.Windows.Forms.TextBox()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Monotype Corsiva", 20.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(66, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(276, 30)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "UTAdvanced Model Data"
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label2.Location = New System.Drawing.Point(74, 102)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(76, 17)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Part Number:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label4.Location = New System.Drawing.Point(74, 137)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(66, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Base Model:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label5.Location = New System.Drawing.Point(32, 62)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(23, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "E1:"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.txtAssy)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.txtS1)
        Me.GroupBox1.Controls.Add(Me.txtE4)
        Me.GroupBox1.Controls.Add(Me.txtE3)
        Me.GroupBox1.Controls.Add(Me.txtE1)
        Me.GroupBox1.Controls.Add(Me.txtE2)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.GroupBox1.Location = New System.Drawing.Point(100, 228)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(209, 242)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Assembly Boards"
        '
        'txtAssy
        '
        Me.txtAssy.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.txtAssy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAssy.Location = New System.Drawing.Point(61, 33)
        Me.txtAssy.Name = "txtAssy"
        Me.txtAssy.ReadOnly = True
        Me.txtAssy.Size = New System.Drawing.Size(133, 20)
        Me.txtAssy.TabIndex = 3
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label10.Location = New System.Drawing.Point(4, 36)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(51, 13)
        Me.Label10.TabIndex = 14
        Me.Label10.Text = "I/O Assy:"
        '
        'txtS1
        '
        Me.txtS1.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.txtS1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtS1.Location = New System.Drawing.Point(61, 198)
        Me.txtS1.Name = "txtS1"
        Me.txtS1.ReadOnly = True
        Me.txtS1.Size = New System.Drawing.Size(134, 20)
        Me.txtS1.TabIndex = 8
        '
        'txtE4
        '
        Me.txtE4.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.txtE4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtE4.Location = New System.Drawing.Point(61, 165)
        Me.txtE4.Name = "txtE4"
        Me.txtE4.ReadOnly = True
        Me.txtE4.Size = New System.Drawing.Size(134, 20)
        Me.txtE4.TabIndex = 7
        '
        'txtE3
        '
        Me.txtE3.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.txtE3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtE3.Location = New System.Drawing.Point(61, 130)
        Me.txtE3.Name = "txtE3"
        Me.txtE3.ReadOnly = True
        Me.txtE3.Size = New System.Drawing.Size(134, 20)
        Me.txtE3.TabIndex = 6
        '
        'txtE1
        '
        Me.txtE1.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.txtE1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtE1.Location = New System.Drawing.Point(61, 62)
        Me.txtE1.Name = "txtE1"
        Me.txtE1.ReadOnly = True
        Me.txtE1.Size = New System.Drawing.Size(134, 20)
        Me.txtE1.TabIndex = 4
        '
        'txtE2
        '
        Me.txtE2.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.txtE2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtE2.Location = New System.Drawing.Point(60, 93)
        Me.txtE2.Name = "txtE2"
        Me.txtE2.ReadOnly = True
        Me.txtE2.Size = New System.Drawing.Size(135, 20)
        Me.txtE2.TabIndex = 5
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ForeColor = System.Drawing.SystemColors.Desktop
        Me.Label9.Location = New System.Drawing.Point(32, 201)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(23, 13)
        Me.Label9.TabIndex = 8
        Me.Label9.Text = "S1:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label8.Location = New System.Drawing.Point(32, 165)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(23, 13)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "E4:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label7.Location = New System.Drawing.Point(32, 130)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(23, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "E3:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label6.Location = New System.Drawing.Point(32, 93)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(23, 13)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "E2:"
        '
        'txtPartNum
        '
        Me.txtPartNum.AcceptsReturn = True
        Me.txtPartNum.AcceptsTab = True
        Me.txtPartNum.Location = New System.Drawing.Point(152, 99)
        Me.txtPartNum.Name = "txtPartNum"
        Me.txtPartNum.Size = New System.Drawing.Size(184, 20)
        Me.txtPartNum.TabIndex = 0
        '
        'txtBaseMod
        '
        Me.txtBaseMod.AccessibleRole = System.Windows.Forms.AccessibleRole.Text
        Me.txtBaseMod.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.txtBaseMod.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtBaseMod.Location = New System.Drawing.Point(152, 133)
        Me.txtBaseMod.Name = "txtBaseMod"
        Me.txtBaseMod.ReadOnly = True
        Me.txtBaseMod.Size = New System.Drawing.Size(184, 20)
        Me.txtBaseMod.TabIndex = 2
        '
        'cmdSubmit
        '
        Me.cmdSubmit.Location = New System.Drawing.Point(31, 508)
        Me.cmdSubmit.Name = "cmdSubmit"
        Me.cmdSubmit.Size = New System.Drawing.Size(99, 31)
        Me.cmdSubmit.TabIndex = 1
        Me.cmdSubmit.Text = "Submit"
        Me.cmdSubmit.UseVisualStyleBackColor = True
        Me.cmdSubmit.Visible = False
        '
        'cmdClear
        '
        Me.cmdClear.Location = New System.Drawing.Point(39, 470)
        Me.cmdClear.Name = "cmdClear"
        Me.cmdClear.Size = New System.Drawing.Size(101, 32)
        Me.cmdClear.TabIndex = 10
        Me.cmdClear.Text = "Clear Results"
        Me.cmdClear.UseVisualStyleBackColor = True
        Me.cmdClear.Visible = False
        '
        'cmdExit
        '
        Me.cmdExit.Location = New System.Drawing.Point(160, 501)
        Me.cmdExit.Name = "cmdExit"
        Me.cmdExit.Size = New System.Drawing.Size(102, 31)
        Me.cmdExit.TabIndex = 11
        Me.cmdExit.Text = "Exit"
        Me.cmdExit.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Location = New System.Drawing.Point(66, 501)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(0, 13)
        Me.Label11.TabIndex = 12
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Location = New System.Drawing.Point(256, 542)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(0, 13)
        Me.Label12.TabIndex = 13
        '
        'txtMSG
        '
        Me.txtMSG.AccessibleRole = System.Windows.Forms.AccessibleRole.Alert
        Me.txtMSG.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.txtMSG.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtMSG.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMSG.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.txtMSG.Location = New System.Drawing.Point(31, 167)
        Me.txtMSG.Multiline = True
        Me.txtMSG.Name = "txtMSG"
        Me.txtMSG.ReadOnly = True
        Me.txtMSG.Size = New System.Drawing.Size(348, 21)
        Me.txtMSG.TabIndex = 7
        Me.txtMSG.TabStop = False
        Me.txtMSG.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtMSG.Visible = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.UTAdvanced.My.Resources.Resources.bkgnd
        Me.ClientSize = New System.Drawing.Size(410, 554)
        Me.Controls.Add(Me.cmdSubmit)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.cmdExit)
        Me.Controls.Add(Me.cmdClear)
        Me.Controls.Add(Me.txtBaseMod)
        Me.Controls.Add(Me.txtMSG)
        Me.Controls.Add(Me.txtPartNum)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "UTAdvanced"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtS1 As System.Windows.Forms.TextBox
    Friend WithEvents txtE4 As System.Windows.Forms.TextBox
    Friend WithEvents txtE3 As System.Windows.Forms.TextBox
    Friend WithEvents txtE2 As System.Windows.Forms.TextBox
    Friend WithEvents txtPartNum As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtE1 As System.Windows.Forms.TextBox
    Friend WithEvents txtBaseMod As System.Windows.Forms.TextBox
    Friend WithEvents cmdSubmit As System.Windows.Forms.Button
    Friend WithEvents cmdClear As System.Windows.Forms.Button
    Friend WithEvents cmdExit As System.Windows.Forms.Button
    Friend WithEvents txtAssy As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtMSG As TextBox
End Class
