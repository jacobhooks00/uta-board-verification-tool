﻿Imports System.Data

Public Class Form1

    Private strModel As String              'CheckSheet용 Model명
    Private strType As String               'Model type, i.e. "UT55A"
    Private strSuffix(7) As String         'Suffix Code
    Private myOptCode(40) As String         'Option Code
    Private myMainAssy As String            'MainBoard Assy No
    Private myPowAssy As String             'PowerBoard Assy No
    Private myIOAssy As String              'IO Board Assy No
    Private myE1Assy As String              'E1 Slot Assy No
    Private myE2Assy As String              'E2 Slot Assy No
    Private myE3Assy As String              'E3 Slot Assy No
    Private myE4Assy As String              'E4 Slot Assy No
    Private myS1Assy As String              'S1 Slot Assy No
    Private myCaseAssy As String            'CaseAssy No
    Private sheetName As String            '형명통달표 Sheet명
    Dim result As Boolean
    Dim CSRow1 As Integer
    Dim CSCol1 As Integer
    Dim i As Integer
    Dim SearchCell As String
    Dim ChkS1 As Boolean = False
    Dim ChkE1 As Boolean = False
    Dim ChkE2 As Boolean = False
    Dim ChkE3 As Boolean = False
    Dim ChkE4 As Boolean = False

    Private CScol2 As Integer               '/DC용 Col -> /DC가 없을시:11, /DC가 있으면:12
    Private CScol3 As Integer               '/LP용 Col -> /LP가 없을시:17, /LP가 있으면:18
    Private CSRol2 As Integer               'MsCode체계에 별 Option에 따른 행값지정 변수
    Private Const inOptMax As Integer = 30
    Dim connstring As String
    Dim myExcelRW As clsExcelRW
    Dim validOpt As Boolean

    'Disable 'X' (exit) button
    Protected Overrides ReadOnly Property CreateParams() As CreateParams
        Get
            Dim param As CreateParams = MyBase.CreateParams
            param.ClassStyle = param.ClassStyle Or &H200
            Return param
        End Get
    End Property

    Function ValidPartNumber(ByVal strModel As String) As Boolean

        ValidPartNumber = True

        If strModel = "" Or strModel.Length() < 15 Then
            txtMSG.Text = "Please enter a valid part number."
            ValidPartNumber = False
            GoTo InvalidExit
        End If

        If Mid(strModel, 6, 1) <> "-" Or Mid(strModel, 10, 1) <> "-" Or Mid(strModel, 13, 1) <> "-" Then
            txtMSG.Text = "Please enter a valid part number."
            ValidPartNumber = False
            GoTo InvalidExit
        End If

        strType = Mid(strModel, 1, 5)

        If strType = "UT75A" Then
            txtMSG.Text = "YCA does not modify UT75A units."
            ValidPartNumber = False
            GoTo InvalidExit
        End If

        If strType <> "UT55A" And strType <> "UT35A" And strType <> "UT52A" And strType <> "UT32A" And strType <> "UP55A" And strType <> "UP35A" And strType <> "UM33A" Then
            txtMSG.Text = "Please enter a valid part number."
            ValidPartNumber = False
            GoTo InvalidExit
        End If

        Select Case Mid(strModel, 9, 1)
            Case 1
                If strType = "UT32A" Then
                    txtMSG.Text = "Type 3 code error. Not a valid unit."
                    ValidPartNumber = False
                    GoTo InvalidExit
                End If
            Case 2
                If strType = "UT32A" Then
                    txtMSG.Text = "Type 3 code error. Not a valid unit."
                    ValidPartNumber = False
                    GoTo InvalidExit
                End If
            Case "3"
                txtMSG.Text = "YCA does not modify CC-Link units."
                ValidPartNumber = False
                GoTo InvalidExit
            Case "4"
                txtMSG.Text = "YCA does not modify PROFIBUS units."
                ValidPartNumber = False
                GoTo InvalidExit
            Case "5"
                txtMSG.Text = "YCA does not modify DeviceNet units."
                ValidPartNumber = False
                GoTo InvalidExit
        End Select

        If InStr(strModel, "/CC3", CompareMethod.Text) Then
            txtMSG.Text = "YCA does not modify CC-Link units."
            ValidPartNumber = False
            GoTo InvalidExit
        End If

        If InStr(strModel, "/PD3", CompareMethod.Text) Then
            txtMSG.Text = "YCA does not modify PROFIBUS units."
            ValidPartNumber = False
            GoTo InvalidExit
        End If

        If InStr(strModel, "/DN3", CompareMethod.Text) Then
            txtMSG.Text = "YCA does not modify DeviceNet units."
            ValidPartNumber = False
            GoTo InvalidExit
        End If

        If Mid(strModel, 7, 1) = "D" Then
            txtMSG.Text = "YCA does not modify Dual Loop units."
            ValidPartNumber = False
            GoTo InvalidExit
        End If

InvalidExit:

    End Function

    Private Sub cmdSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSubmit.Click

    End Sub
    '=================================================================
    Private Sub DeviceMSCode(ByVal myMs_Code As String)

        Dim i As Integer

        For i = 0 To 7
            strSuffix(i) = ""
        Next

        For i = 0 To 40
            myOptCode(i) = ""
        Next

        CScol2 = 11
        CScol3 = 17

        strType = Mid(myMs_Code, 1, 5)
        strSuffix(1) = Mid(myMs_Code, 7, 1)
        strSuffix(2) = Mid(myMs_Code, 8, 1)

        strSuffix(3) = Mid(myMs_Code, 9, 1)
        strSuffix(4) = Mid(myMs_Code, 11, 1)
        strSuffix(5) = Mid(myMs_Code, 12, 1)
        strSuffix(6) = Mid(myMs_Code, 14, 1)
        strSuffix(7) = Mid(myMs_Code, 15, 1)

        Select Case strType
            Case "UT52A"
                Select Case strSuffix(1)
                    Case "N"
                        sheetName = "UT52ADetail"
                        If InStr(myMs_Code, "/DC", CompareMethod.Text) > 0 Then
                            myOptCode(0) = "/DC"
                            CScol2 = 12
                        End If
                        If InStr(myMs_Code, "/CT", CompareMethod.Text) > 0 Then
                            myOptCode(1) = "/CT"
                        End If
                        If InStr(myMs_Code, "/HA", CompareMethod.Text) > 0 Then
                            myOptCode(2) = "/HA"
                        End If
                        If InStr(myMs_Code, "/RT", CompareMethod.Text) > 0 Then
                            myOptCode(3) = "/RT"
                        End If
                        If InStr(myMs_Code, "/R1", CompareMethod.Text) > 0 Then
                            myOptCode(4) = "/R1"
                        End If
                        If InStr(myMs_Code, "/U1", CompareMethod.Text) > 0 Then
                            myOptCode(5) = "/U1"
                        End If
                        If InStr(myMs_Code, "/L1", CompareMethod.Text) > 0 Then
                            myOptCode(6) = "/L1"
                        End If
                        If InStr(myMs_Code, "/CH1", CompareMethod.Text) > 0 Then
                            myOptCode(7) = "/CH1"
                        End If
                        If InStr(myMs_Code, "/RCH1", CompareMethod.Text) > 0 Then
                            myOptCode(8) = "/RCH1"
                        End If
                        If InStr(myMs_Code, "/LCH1", CompareMethod.Text) > 0 Then
                            myOptCode(9) = "/LCH1"
                        End If
                        If InStr(myMs_Code, "/X1", CompareMethod.Text) > 0 Then
                            myOptCode(10) = "/X1"
                        End If
                        If InStr(myMs_Code, "/Y1", CompareMethod.Text) > 0 Then
                            myOptCode(11) = "/Y1"
                        End If
                        If InStr(myMs_Code, "/W1", CompareMethod.Text) > 0 Then
                            myOptCode(12) = "/W1"
                        End If
                    Case Else
                        sheetName = "UT52AStandard"
                        If InStr(myMs_Code, "/DR", CompareMethod.Text) > 0 Then
                            myOptCode(0) = "/DR"
                        End If
                        If InStr(myMs_Code, "/LP", CompareMethod.Text) > 0 Then
                            myOptCode(1) = "/LP"
                        End If
                        If InStr(myMs_Code, "/DC", CompareMethod.Text) > 0 Then
                            myOptCode(2) = "/DC"
                        End If
                        If InStr(myMs_Code, "/CT", CompareMethod.Text) > 0 Then
                            myOptCode(3) = "/CT"
                        End If
                        If InStr(myMs_Code, "/HA", CompareMethod.Text) > 0 Then
                            myOptCode(4) = "/HA"
                        End If
                        If InStr(myMs_Code, "/MD", CompareMethod.Text) > 0 Then
                        End If
                End Select
            Case "UT55A"
                Select Case strSuffix(1)
                    Case "N"
                        sheetName = "UT55ADetail"
                        If InStr(myMs_Code, "/DC", CompareMethod.Text) > 0 Then
                            myOptCode(0) = "/DC"
                            CScol2 = 12
                        End If
                        If InStr(myMs_Code, "/CT", CompareMethod.Text) > 0 Then
                            myOptCode(1) = "/CT"
                        End If
                        If InStr(myMs_Code, "/MDL", CompareMethod.Text) > 0 Then
                            txtMSG.Text = "YCA Does not modify these units!"
                        End If
                        If InStr(myMs_Code, "/HA", CompareMethod.Text) > 0 Then
                            myOptCode(2) = "/HA"
                        End If
                        If InStr(myMs_Code, "/RT", CompareMethod.Text) > 0 Then
                            myOptCode(3) = "/RT"
                        End If
                        If InStr(myMs_Code, "/R1", CompareMethod.Text) > 0 Then
                            myOptCode(4) = "/R1"
                        End If
                        If InStr(myMs_Code, "/U1", CompareMethod.Text) > 0 Then
                            myOptCode(5) = "/U1"
                        End If
                        If InStr(myMs_Code, "/X1", CompareMethod.Text) > 0 Then
                            myOptCode(6) = "/X1"
                        End If
                        If InStr(myMs_Code, "/Y1", CompareMethod.Text) > 0 Then
                            myOptCode(7) = "/Y1"
                        End If
                        If InStr(myMs_Code, "/W1", CompareMethod.Text) > 0 Then
                            myOptCode(8) = "/W1"
                        End If
                        If InStr(myMs_Code, "/A2", CompareMethod.Text) > 0 Then
                            myOptCode(9) = "/A2"
                        End If
                        If InStr(myMs_Code, "/X2", CompareMethod.Text) > 0 Then
                            myOptCode(10) = "/X2"
                        End If
                        If InStr(myMs_Code, "/Y2", CompareMethod.Text) > 0 Then
                            myOptCode(11) = "/Y2"
                        End If
                        If InStr(myMs_Code, "/W2", CompareMethod.Text) > 0 Then
                            myOptCode(12) = "/W2"
                        End If
                        If InStr(myMs_Code, "/CH3", CompareMethod.Text) > 0 Then
                            myOptCode(13) = "/CH3"
                        End If
                        If InStr(myMs_Code, "/CC3", CompareMethod.Text) > 0 Then
                            myOptCode(14) = "/CC3"
                        End If
                        If InStr(myMs_Code, "/PD3", CompareMethod.Text) > 0 Then
                            myOptCode(15) = "/PD3"
                        End If
                        If InStr(myMs_Code, "/DN3", CompareMethod.Text) > 0 Then
                            myOptCode(16) = "/DN3"
                        End If
                        If InStr(myMs_Code, "/ET3", CompareMethod.Text) > 0 Then
                            myOptCode(17) = "/ET3"
                        End If
                        If InStr(myMs_Code, "/X3", CompareMethod.Text) > 0 Then
                            myOptCode(18) = "/X3"
                        End If
                        If InStr(myMs_Code, "/Y3", CompareMethod.Text) > 0 Then
                            myOptCode(19) = "/Y3"
                        End If
                        If InStr(myMs_Code, "/W3", CompareMethod.Text) > 0 Then
                            myOptCode(20) = "/W3"
                        End If
                        If InStr(myMs_Code, "/A4", CompareMethod.Text) > 0 Then
                            myOptCode(21) = "/A4"
                        End If
                        If InStr(myMs_Code, "/C4", CompareMethod.Text) > 0 Then
                            myOptCode(22) = "/C4"
                        End If
                        If InStr(myMs_Code, "/L4", CompareMethod.Text) > 0 Then
                            myOptCode(23) = "/L4"
                        End If
                        If InStr(myMs_Code, "/AC4", CompareMethod.Text) > 0 Then
                            myOptCode(24) = "/AC4"
                        End If
                        If InStr(myMs_Code, "/LC4", CompareMethod.Text) > 0 Then
                            myOptCode(25) = "/LC4"
                        End If
                        If InStr(myMs_Code, "/X4", CompareMethod.Text) > 0 Then
                            myOptCode(26) = "/X4"
                        End If
                        If InStr(myMs_Code, "/Y4", CompareMethod.Text) > 0 Then
                            myOptCode(27) = "/Y4"
                        End If
                        If InStr(myMs_Code, "/W4", CompareMethod.Text) > 0 Then
                            myOptCode(28) = "/W4"
                        End If
                    Case Else
                        sheetName = "UT55AStandard"
                        If InStr(myMs_Code, "/DR", CompareMethod.Text) > 0 Then
                            myOptCode(0) = "/DR"
                        End If
                        If InStr(myMs_Code, "/LP", CompareMethod.Text) > 0 Then
                            myOptCode(1) = "/LP"
                            CScol3 = 18
                        End If
                        If InStr(myMs_Code, "/DC", CompareMethod.Text) > 0 Then
                            myOptCode(2) = "/DC"
                            CScol2 = 12
                        End If
                        If InStr(myMs_Code, "/CT", CompareMethod.Text) > 0 Then
                            myOptCode(3) = "/CT"
                        End If
                        If InStr(myMs_Code, "/HA", CompareMethod.Text) > 0 Then
                            myOptCode(4) = "/HA"
                            CScol3 = 13
                        End If
                        If InStr(myMs_Code, "/MD", CompareMethod.Text) > 0 Then
                        End If
                End Select
            Case "UT35A"
                Select Case strSuffix(1)
                    Case "N"
                        sheetName = "UT35ADetail"
                        If InStr(myMs_Code, "/DC", CompareMethod.Text) > 0 Then
                            myOptCode(0) = "/DC"
                        End If
                        If InStr(myMs_Code, "/CT", CompareMethod.Text) > 0 Then
                            myOptCode(1) = "/CT"
                        End If
                        If InStr(myMs_Code, "/HA", CompareMethod.Text) > 0 Then
                            myOptCode(2) = "/HA"
                        End If
                        If InStr(myMs_Code, "/RT", CompareMethod.Text) > 0 Then
                            myOptCode(3) = "/RT"
                        End If
                        If InStr(myMs_Code, "/X1", CompareMethod.Text) > 0 Then
                            myOptCode(4) = "/X1"
                        End If
                        If InStr(myMs_Code, "/Y1", CompareMethod.Text) > 0 Then
                            myOptCode(5) = "/Y1"
                        End If
                        If InStr(myMs_Code, "/W1", CompareMethod.Text) > 0 Then
                            myOptCode(6) = "/W1"
                        End If
                        If InStr(myMs_Code, "/CH3", CompareMethod.Text) > 0 Then
                            myOptCode(7) = "/CH3"
                        End If
                        If InStr(myMs_Code, "/CC3", CompareMethod.Text) > 0 Then
                            myOptCode(8) = "/CC3"
                        End If
                        If InStr(myMs_Code, "/PD3", CompareMethod.Text) > 0 Then
                            myOptCode(9) = "/PD3"
                        End If
                        If InStr(myMs_Code, "/DN3", CompareMethod.Text) > 0 Then
                            myOptCode(10) = "/DN3"
                        End If
                        If InStr(myMs_Code, "/ET3", CompareMethod.Text) > 0 Then
                            myOptCode(11) = "/ET3"
                        End If
                        If InStr(myMs_Code, "/L4", CompareMethod.Text) > 0 Then
                            myOptCode(12) = "/L4"
                        End If
                        If InStr(myMs_Code, "/X4", CompareMethod.Text) > 0 Then
                            myOptCode(13) = "/X4"
                        End If
                        If InStr(myMs_Code, "/Y4", CompareMethod.Text) > 0 Then
                            myOptCode(14) = "/Y4"
                        End If
                        If InStr(myMs_Code, "/W4", CompareMethod.Text) > 0 Then
                            myOptCode(15) = "/W4"
                        End If
                    Case Else
                        sheetName = "UT35AStandard"
                        If InStr(myMs_Code, "/LP", CompareMethod.Text) > 0 Then
                            myOptCode(0) = "/LP"
                            CScol3 = 18
                        End If
                        If InStr(myMs_Code, "/DC", CompareMethod.Text) > 0 Then
                            myOptCode(1) = "/DC"
                            CScol2 = 12
                        End If
                        If InStr(myMs_Code, "/CT", CompareMethod.Text) > 0 Then
                            myOptCode(2) = "/CT"
                        End If
                        If InStr(myMs_Code, "/HA", CompareMethod.Text) > 0 Then
                            myOptCode(3) = "/HA"
                        End If
                        If InStr(myMs_Code, "/MD", CompareMethod.Text) > 0 Then
                        End If
                End Select
            Case "UT32A"
                Select Case strSuffix(1)
                    Case "N"
                        sheetName = "UT32ADetail"
                        If InStr(myMs_Code, "/DC", CompareMethod.Text) > 0 Then
                            myOptCode(0) = "/DC"
                            CScol2 = 12
                        End If
                        If InStr(myMs_Code, "/CT", CompareMethod.Text) > 0 Then
                            myOptCode(1) = "/CT"
                        End If
                        If InStr(myMs_Code, "/HA", CompareMethod.Text) > 0 Then
                            myOptCode(2) = "/HA"
                        End If
                        If InStr(myMs_Code, "/RT", CompareMethod.Text) > 0 Then
                            myOptCode(3) = "/RT"
                        End If
                        If InStr(myMs_Code, "/L1", CompareMethod.Text) > 0 Then
                            myOptCode(4) = "/L1"
                        End If
                        If InStr(myMs_Code, "/CH1", CompareMethod.Text) > 0 Then
                            myOptCode(5) = "/CH1"
                        End If
                        If InStr(myMs_Code, "/LCH1", CompareMethod.Text) > 0 Then
                            myOptCode(6) = "/LCH1"
                        End If
                        If InStr(myMs_Code, "/X1", CompareMethod.Text) > 0 Then
                            myOptCode(7) = "/X1"
                        End If
                        If InStr(myMs_Code, "/Y1", CompareMethod.Text) > 0 Then
                            myOptCode(8) = "/Y1"
                        End If
                        If InStr(myMs_Code, "/W1", CompareMethod.Text) > 0 Then
                            myOptCode(9) = "/W1"
                        End If
                    Case Else
                        sheetName = "UT32AStandard"
                        If InStr(myMs_Code, "/LP", CompareMethod.Text) > 0 Then
                            myOptCode(0) = "/LP"
                            CScol3 = 18
                        End If
                        If InStr(myMs_Code, "/DC", CompareMethod.Text) > 0 Then
                            myOptCode(1) = "/DC"
                            CScol2 = 12
                        End If
                        If InStr(myMs_Code, "/CT", CompareMethod.Text) > 0 Then
                            myOptCode(2) = "/CT"
                        End If
                        If InStr(myMs_Code, "/HA", CompareMethod.Text) > 0 Then
                            myOptCode(3) = "/HA"
                            CScol2 = 13
                        End If
                        If InStr(myMs_Code, "/MD", CompareMethod.Text) > 0 Then
                        End If
                End Select
                '----------------- Rev 0.08 ADD ----------------
            Case "UP55A"
                Select Case strSuffix(1)
                    Case "N"
                        sheetName = "UP55ADetail"
                        If InStr(myMs_Code, "/DC", CompareMethod.Text) > 0 Then
                            myOptCode(0) = "/DC"
                            CScol2 = 12
                        End If
                        If InStr(myMs_Code, "/CT", CompareMethod.Text) > 0 Then
                            myOptCode(1) = "/CT"
                        End If
                        If InStr(myMs_Code, "/HA", CompareMethod.Text) > 0 Then
                            myOptCode(2) = "/HA"
                        End If
                        If InStr(myMs_Code, "/RT", CompareMethod.Text) > 0 Then
                            myOptCode(3) = "/RT"
                        End If
                        If InStr(myMs_Code, "/R1", CompareMethod.Text) > 0 Then
                            myOptCode(4) = "/R1"
                        End If
                        If InStr(myMs_Code, "/U1", CompareMethod.Text) > 0 Then
                            myOptCode(5) = "/U1"
                        End If
                        If InStr(myMs_Code, "/X1", CompareMethod.Text) > 0 Then
                            myOptCode(6) = "/X1"
                        End If
                        If InStr(myMs_Code, "/Y1", CompareMethod.Text) > 0 Then
                            myOptCode(7) = "/Y1"
                        End If
                        If InStr(myMs_Code, "/W1", CompareMethod.Text) > 0 Then
                            myOptCode(8) = "/W1"
                        End If
                        If InStr(myMs_Code, "/A2", CompareMethod.Text) > 0 Then
                            myOptCode(9) = "/A2"
                        End If
                        If InStr(myMs_Code, "/X2", CompareMethod.Text) > 0 Then
                            myOptCode(10) = "/X2"
                        End If
                        If InStr(myMs_Code, "/Y2", CompareMethod.Text) > 0 Then
                            myOptCode(11) = "/Y2"
                        End If
                        If InStr(myMs_Code, "/W2", CompareMethod.Text) > 0 Then
                            myOptCode(12) = "/W2"
                        End If
                        If InStr(myMs_Code, "/CH3", CompareMethod.Text) > 0 Then
                            myOptCode(13) = "/CH3"
                        End If
                        If InStr(myMs_Code, "/CC3", CompareMethod.Text) > 0 Then
                            myOptCode(14) = "/CC3"
                        End If
                        If InStr(myMs_Code, "/PD3", CompareMethod.Text) > 0 Then
                            myOptCode(15) = "/PD3"
                        End If
                        If InStr(myMs_Code, "/DN3", CompareMethod.Text) > 0 Then
                            myOptCode(16) = "/DN3"
                        End If
                        If InStr(myMs_Code, "/ET3", CompareMethod.Text) > 0 Then
                            myOptCode(17) = "/ET3"
                        End If
                        If InStr(myMs_Code, "/X3", CompareMethod.Text) > 0 Then
                            myOptCode(18) = "/X3"
                        End If
                        If InStr(myMs_Code, "/Y3", CompareMethod.Text) > 0 Then
                            myOptCode(19) = "/Y3"
                        End If
                        If InStr(myMs_Code, "/W3", CompareMethod.Text) > 0 Then
                            myOptCode(20) = "/W3"
                        End If
                        If InStr(myMs_Code, "/A4", CompareMethod.Text) > 0 Then
                            myOptCode(21) = "/A4"
                        End If
                        If InStr(myMs_Code, "/C4", CompareMethod.Text) > 0 Then
                            myOptCode(22) = "/C4"
                        End If
                        If InStr(myMs_Code, "/L4", CompareMethod.Text) > 0 Then
                            myOptCode(23) = "/L4"
                        End If
                        If InStr(myMs_Code, "/AC4", CompareMethod.Text) > 0 Then
                            myOptCode(24) = "/AC4"
                        End If
                        If InStr(myMs_Code, "/LC4", CompareMethod.Text) > 0 Then
                            myOptCode(25) = "/LC4"
                        End If
                        If InStr(myMs_Code, "/X4", CompareMethod.Text) > 0 Then
                            myOptCode(26) = "/X4"
                        End If
                        If InStr(myMs_Code, "/Y4", CompareMethod.Text) > 0 Then
                            myOptCode(27) = "/Y4"
                        End If
                        If InStr(myMs_Code, "/W4", CompareMethod.Text) > 0 Then
                            myOptCode(28) = "/W4"
                        End If
                    Case Else
                        sheetName = "UP55AStandard"
                        If InStr(myMs_Code, "/DR", CompareMethod.Text) > 0 Then
                            myOptCode(0) = "/DR"
                        End If
                        If InStr(myMs_Code, "/DC", CompareMethod.Text) > 0 Then
                            myOptCode(1) = "/DC"
                            CScol2 = 12
                        End If
                        If InStr(myMs_Code, "/CT", CompareMethod.Text) > 0 Then
                            myOptCode(2) = "/CT"
                        End If
                        If InStr(myMs_Code, "/HA", CompareMethod.Text) > 0 Then
                            myOptCode(3) = "/HA"
                        End If
                        If InStr(myMs_Code, "/MD", CompareMethod.Text) > 0 Then
                        End If
                End Select
            Case "UP35A"
                Select Case strSuffix(1)
                    Case "N"
                        sheetName = "UP35ADetail"
                        If InStr(myMs_Code, "/DC", CompareMethod.Text) > 0 Then
                            myOptCode(0) = "/DC"
                            CScol2 = 12
                        End If
                        If InStr(myMs_Code, "/CT", CompareMethod.Text) > 0 Then
                            myOptCode(1) = "/CT"
                        End If
                        If InStr(myMs_Code, "/HA", CompareMethod.Text) > 0 Then
                            myOptCode(2) = "/HA"
                        End If
                        If InStr(myMs_Code, "/AP", CompareMethod.Text) > 0 Then
                            myOptCode(3) = "/AP"
                        End If
                        If InStr(myMs_Code, "/RT", CompareMethod.Text) > 0 Then
                            myOptCode(4) = "/RT"
                        End If
                        If InStr(myMs_Code, "/X1", CompareMethod.Text) > 0 Then
                            myOptCode(5) = "/X1"
                        End If
                        If InStr(myMs_Code, "/Y1", CompareMethod.Text) > 0 Then
                            myOptCode(6) = "/Y1"
                        End If
                        If InStr(myMs_Code, "/W1", CompareMethod.Text) > 0 Then
                            myOptCode(7) = "/W1"
                        End If
                        If InStr(myMs_Code, "/CH3", CompareMethod.Text) > 0 Then
                            myOptCode(8) = "/CH3"
                        End If
                        If InStr(myMs_Code, "/CC3", CompareMethod.Text) > 0 Then
                            myOptCode(9) = "/CC3"
                        End If
                        If InStr(myMs_Code, "/PD3", CompareMethod.Text) > 0 Then
                            myOptCode(10) = "/PD3"
                        End If
                        If InStr(myMs_Code, "/DN3", CompareMethod.Text) > 0 Then
                            myOptCode(11) = "/DN3"
                        End If
                        If InStr(myMs_Code, "/ET3", CompareMethod.Text) > 0 Then
                            myOptCode(12) = "/ET3"
                        End If
                        If InStr(myMs_Code, "/L4", CompareMethod.Text) > 0 Then
                            myOptCode(13) = "/L4"
                        End If
                        If InStr(myMs_Code, "/X4", CompareMethod.Text) > 0 Then
                            myOptCode(14) = "/X4"
                        End If
                        If InStr(myMs_Code, "/Y4", CompareMethod.Text) > 0 Then
                            myOptCode(15) = "/Y4"
                        End If
                        If InStr(myMs_Code, "/W4", CompareMethod.Text) > 0 Then
                            myOptCode(16) = "/W4"
                        End If
                    Case Else
                        sheetName = "UP35AStandard"
                        If InStr(myMs_Code, "/DC", CompareMethod.Text) > 0 Then
                            myOptCode(0) = "/DC"
                            ' CScol2 = 12
                        End If
                        If InStr(myMs_Code, "/CT", CompareMethod.Text) > 0 Then
                            myOptCode(1) = "/CT"
                        End If
                        If InStr(myMs_Code, "/HA", CompareMethod.Text) > 0 Then
                            myOptCode(2) = "/HA"
                        End If
                        If InStr(myMs_Code, "/AP", CompareMethod.Text) > 0 Then
                            myOptCode(3) = "/AP"
                        End If
                        If InStr(myMs_Code, "/MD", CompareMethod.Text) > 0 Then
                        End If
                End Select
            Case "UM33A"
                sheetName = "UM33AStandard"
                If InStr(myMs_Code, "/LP", CompareMethod.Text) > 0 Then
                    myOptCode(0) = "/LP"
                    CScol3 = 15
                End If
                If InStr(myMs_Code, "/DC", CompareMethod.Text) > 0 Then
                    myOptCode(1) = "/DC"
                    CScol2 = 12
                End If
                If InStr(myMs_Code, "/CT", CompareMethod.Text) > 0 Then
                    myOptCode(2) = "/CT"
                End If
                If InStr(myMs_Code, "/MD", CompareMethod.Text) > 0 Then
                End If
        End Select

        result = myExcelRW.SelectSheet(sheetName)

        'Excel Sheet
        If result = False Then
            Throw New System.ApplicationException("Excel Sheet Read Error")
            myExcelRW.QuitExcel()
            myExcelRW = Nothing
        End If

        For Me.CSCol1 = 2 To 8
            CSRow1 = 8
            Do
                SearchCell = myExcelRW.GetDataFromCell(CSRow1, CSCol1)  'MS-CODE 통지표의 해당 CELL값을 가져옴
                CSRow1 = CSRow1 + 1                                     '(Bringing a notice ideographic corresponding CELL price,)
                If CSRow1 = 60 Then
                    Exit Do
                End If
            Loop Until strSuffix(CSCol1 - 1) = SearchCell
            'I/O assy value
            If myExcelRW.GetDataFromCell(CSRow1 - 1, 10) <> "" Then
                myIOAssy = myExcelRW.GetDataFromCell(CSRow1 - 1, 10)
                txtAssy.Text = myIOAssy
            End If
            If myExcelRW.GetDataFromCell(CSRow1 - 1, 13) <> "" Then
                myS1Assy = myExcelRW.GetDataFromCell(CSRow1 - 1, 13)
                txtS1.Text = myS1Assy
                ChkS1 = True
            End If
            '-----------< ADD 0.08 >------------
            If strType = "UM33A" And InStr(myMs_Code, "/LP", CompareMethod.Text) > 0 Then
                If myExcelRW.GetDataFromCell(CSRow1 - 1, 15) <> "" Then
                    myE1Assy = myExcelRW.GetDataFromCell(CSRow1 - 1, 15)
                    txtE1.Text = myE1Assy
                    ChkE1 = True
                End If
            Else
                If myExcelRW.GetDataFromCell(CSRow1 - 1, 14) <> "" Then
                    myE1Assy = myExcelRW.GetDataFromCell(CSRow1 - 1, 14)
                    txtE1.Text = myE1Assy
                    ChkE1 = True
                End If
            End If
            '-----------------------------------
            If strType = "UT55A" Or strType = "UT35A" Or strType = "UP55A" Or strType = "UP35A" Then
                If myExcelRW.GetDataFromCell(CSRow1 - 1, 15) <> "" Then
                    myE2Assy = myExcelRW.GetDataFromCell(CSRow1 - 1, 15)
                    txtE2.Text = myE2Assy
                    ChkE2 = True
                End If
                If myExcelRW.GetDataFromCell(CSRow1 - 1, 16) <> "" Then
                    myE3Assy = myExcelRW.GetDataFromCell(CSRow1 - 1, 16)
                    ChkE3 = True
                    txtE3.Text = myE3Assy
                End If
                Select Case strSuffix(2)
                    Case "0", "1", "2", "3", "4"
                        If strType = "UT35A" And InStr(myMs_Code, "/LP") Then
                            If myExcelRW.GetDataFromCell(CSRow1 - 1, CScol3) <> "" Then
                                myE4Assy = myExcelRW.GetDataFromCell(CSRow1 - 1, CScol3)
                                txtE4.Text = myE4Assy
                                ChkE4 = True
                            End If
                        Else
                            If myExcelRW.GetDataFromCell(CSRow1 - 1, CScol3) <> "" Then
                                myE4Assy = myExcelRW.GetDataFromCell(CSRow1 - 1, CScol3)
                                txtE4.Text = myE4Assy
                                ChkE4 = True
                            End If
                        End If
                    Case Else
                        If myExcelRW.GetDataFromCell(CSRow1 - 1, 17) <> "" Then
                            myE4Assy = myExcelRW.GetDataFromCell(CSRow1 - 1, 17)
                            txtE4.Text = myE4Assy
                            ChkE4 = True
                        End If
                End Select
                If strType = "UT55A" And InStr(myMs_Code, "/LP") Then
                    Select Case strSuffix(2)
                        Case 0, 3, 4
                            'row = 11,14,15
                            If myExcelRW.GetDataFromCell(CSRow1 - 1, CScol3) <> "" Then
                                myE4Assy = myExcelRW.GetDataFromCell(CSRow1 - 1, CScol3)
                                txtE4.Text = myE4Assy
                                ChkE4 = True
                            End If
                            If myExcelRW.GetDataFromCell(CSRow1 - 2, CScol3) <> "" Then
                                myE3Assy = myExcelRW.GetDataFromCell(CSRow1 - 2, CScol2)
                                txtE3.Text = myE3Assy
                                ChkE3 = True
                            End If
                        Case 1, 2
                            'row = 12 or 13
                            If myExcelRW.GetDataFromCell(CSRow1 - 1, CScol3) <> "" Then
                                myE4Assy = myExcelRW.GetDataFromCell(CSRow1 - 1, CScol3)
                                txtE4.Text = myE4Assy
                                ChkE4 = True
                            End If
                    End Select
                End If
            End If

        Next CSCol1

        '--------< Option Code에 따른 Assy정보 취득 >--------
        CSRow1 = 8
        Do                                                          'Option Code가 시작되는 행값을 취득 (The line price which is started acquisition)
            SearchCell = myExcelRW.GetDataFromCell(CSRow1, 9)
            CSRow1 = CSRow1 + 1
            If CSRow1 = 60 Then
                Exit Do
            End If
        Loop Until SearchCell = "/DC" Or SearchCell = "/DR" Or SearchCell = "/LP"

        For i = 0 To inOptMax

            If myOptCode(i) = myExcelRW.GetDataFromCell(CSRow1 - 1 + i, 9) Then

                If myExcelRW.GetDataFromCell(CSRow1 - 1 + i, 10) <> "" Then
                    myIOAssy = myExcelRW.GetDataFromCell(CSRow1 - 1 + i, 10)
                End If

                If myExcelRW.GetDataFromCell(CSRow1 - 1 + i, 13) <> "" Then
                    myS1Assy = myExcelRW.GetDataFromCell(CSRow1 - 1 + i, 13)
                    ChkS1 = True
                    If InStr(myMs_Code, "/HA", CompareMethod.Text) > 0 Then
                        txtS1.Text = myS1Assy  'For some reason, this S1 board no longer comes in controller  AR 9/22/2016
                    End If
                End If
                If myExcelRW.GetDataFromCell(CSRow1 - 1 + i, 14) <> "" Then
                    myE1Assy = myExcelRW.GetDataFromCell(CSRow1 - 1 + i, 14)
                    '#minami add-----------------------------------------------#
                    If strType = "UT32A" Then
                        If Not (strSuffix(1) = "N") Then
                            If strSuffix(2) = "1" And InStr(myMs_Code, "/LP", CompareMethod.Text) > 0 Then
                                myE1Assy = "L3502MG"
                            End If
                        End If
                        If strSuffix(1) = "0" Then
                            myE2Assy = "L3503BG"
                        End If
                    End If
                    '#minami add----------------------------------------------#
                    txtE1.Text = myE1Assy
                    ChkE1 = True
                End If

                If strType = "UT55A" Or strType = "UT35A" Or strType = "UP55A" Or strType = "UP35A" Then    'CHG 0.08
                    If myExcelRW.GetDataFromCell(CSRow1 - 1 + i, 15) <> "" Then
                        myE2Assy = myExcelRW.GetDataFromCell(CSRow1 - 1 + i, 15)
                        txtE2.Text = myE2Assy
                        ChkE2 = True
                    End If
                    If strType = "UT35A" Then
                        If myExcelRW.GetDataFromCell(CSRow1 - 1 + i, 15) <> "" Then
                            myE2Assy = myExcelRW.GetDataFromCell(15 - 1 + i, 15)
                        End If
                    End If
                    If myExcelRW.GetDataFromCell(CSRow1 - 1 + i, 16) <> "" Then
                        myE3Assy = myExcelRW.GetDataFromCell(CSRow1 - 1 + i, 16)
                        txtE3.Text = myE3Assy
                        ChkE3 = True
                    End If
                    If myExcelRW.GetDataFromCell(CSRow1 - 1 + i, CScol3) <> "" Then
                        myE4Assy = myExcelRW.GetDataFromCell(CSRow1 - 1 + i, CScol3)
                        txtE4.Text = myE4Assy
                        ChkE4 = True
                    End If
                End If
            End If
        Next i

EndLine:

    End Sub

    Private Sub cmdClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClear.Click

        'Clear textboxes
        txtE1.Text = vbNullString
        txtS1.Text = vbNullString
        txtMSG.Text = vbNullString
        txtBaseMod.Text = vbNullString
        txtAssy.Text = vbNullString
        txtE2.Text = vbNullString
        txtE3.Text = vbNullString
        txtE4.Text = vbNullString

        'Set focus at part number textbox
        txtPartNum.Focus()

    End Sub

    Private Sub cmdExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdExit.Click

        myExcelRW.CloseWorkBook()
        myExcelRW.QuitExcel()

        Close()

    End Sub

    Private Sub GroupBox1_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub txtPartNum_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPartNum.Click
        'Clear textboxes
        txtE1.Text = vbNullString
        txtS1.Text = vbNullString
        txtMSG.Text = vbNullString
        txtBaseMod.Text = vbNullString
        txtAssy.Text = vbNullString
        txtE2.Text = vbNullString
        txtE3.Text = vbNullString
        txtE4.Text = vbNullString

    End Sub

    Private Sub txtPartNum_TabIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPartNum.TabIndexChanged

    End Sub

    Protected Overrides Sub Finalize()

        MyBase.Finalize()

    End Sub

    Public Sub New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        myExcelRW = New clsExcelRW
        myExcelRW.OpenWorkBook("\\svr300-003\mfg\Deployed Programs\UTAdvanced\CheckSheet\UTxA CheckSheet.xls")

    End Sub
    Private Sub txtBaseMod_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBaseMod.TextChanged

    End Sub

    Private Sub txtPartNum_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPartNum.TextChanged

        txtE1.Text = vbNullString
        txtS1.Text = vbNullString
        txtMSG.Text = vbNullString
        txtBaseMod.Text = vbNullString
        txtAssy.Text = vbNullString
        txtE2.Text = vbNullString
        txtE3.Text = vbNullString
        txtE4.Text = vbNullString

        txtMSG.Visible = False

        Dim strModel As String
        strModel = UCase(txtPartNum.Text)

        'Per Production Control AR 9/12/
        If InStr(strModel, "/S046") Or InStr(strModel, "/MDL") Or InStr(strModel, "/RSP") Or InStr(strModel, "/DR") Or InStr(strModel, "/CT") Or InStr(strModel, "/DC") Then
            txtMSG.ForeColor = Color.Red
            txtMSG.Visible = True
            txtMSG.Text = "Check stock or order unit from Japan."
        End If

        If InStr(strModel, "/RSP") Then
            txtMSG.ForeColor = Color.Red
            txtMSG.Visible = True
            txtMSG.Text = "Order unit from Japan."
        End If

        If strModel.Length() = 15 Or strModel.Length() >= 18 Then
            Dim strBase As String = ""
            Dim myMainAssy As String
            Dim intTemp As Integer

            If Not ValidPartNumber(strModel) Then
                txtMSG.ForeColor = Color.Red
                txtMSG.Visible = True
                cmdClear.PerformClick()
                GoTo EndLine
            ElseIf strType = "UP55A" And Mid(strModel, 8, 1) <> "4" Then
                txtMSG.Text = "JG(E2) & JH(E4) already installed" 'JG and JH boards usually included in UP55A units
                txtMSG.ForeColor = Color.Red
                txtMSG.Visible = True
            End If

            strType = Mid(strModel, 1, 5)
            intTemp = Mid(strModel, 12, 1)

            'If /DC mod & places 8 and 9 are other than 0, show DC in base model code
            Select Case Mid(strModel, 7, 1)
                Case "0"
                    If intTemp = 0 Then
                        If InStr(strModel, "DC") = False Then
                            Select Case strType
                                Case "UT32A"
                                    strBase = "UT32A-000-10-00"
                                Case "UT35A"
                                    strBase = "UT35A-000-10-00"
                                Case "UT52A"
                                    strBase = "UT52A-000-10-00"
                                Case "UT55A"
                                    strBase = "UT55A-000-10-00"
                                Case "UP55A"
                                    strBase = "UP55A-000-10-00"
                                Case "UP35A"
                                    strBase = "UP35A-000-10-00"
                                Case "UM33A"
                                    strBase = "UM33A-000-10-00"
                            End Select
                        Else 'dc true
                            txtMSG.Text = "Confirm /DC base unit is in stock."
                            Select Case strType
                                Case "UT32A"
                                    strBase = "UT32A-000-10-00/DC"
                                Case "UT35A"
                                    strBase = "UT35A-000-10-00/DC"
                                Case "UT52A"
                                    strBase = "UT52A-000-10-00/DC"
                                Case "UT55A"
                                    strBase = "UT55A-000-10-00/DC"
                                Case "UP55A"
                                    strBase = "UP55A-000-10-00/DC"
                                Case "UP35A"
                                    strBase = "UP35A-000-10-00/DC"
                                Case "UM33A"
                                    strBase = "UM33A-000-10-00/DC"
                            End Select
                        End If
                    Else
                        If InStr(strModel, "DC") = False Then
                            Select Case strType
                                Case "UT32A"
                                    strBase = "UT32A-000-11-00"
                                Case "UT35A"
                                    strBase = "UT35A-000-11-00"
                                Case "UT52A"
                                    strBase = "UT52A-000-11-00"
                                Case "UT55A"
                                    strBase = "UT55A-000-11-00"
                                Case "UP55A"
                                    strBase = "UP55A-000-11-00"
                                Case "UP35A"
                                    strBase = "UP35A-000-11-00"
                                Case "UM33A"
                                    strBase = "UM33A-000-11-00"
                            End Select
                        Else 'if dc = true
                            txtMSG.Text = "Confirm /DC base unit is in stock."
                            Select Case strType
                                Case "UT32A" : strBase = "UT32A-000-11-00/DC"
                                Case "UT35A"
                                    strBase = "UT35A-000-11-00/DC"
                                Case "UT52A"
                                    strBase = "UT52A-000-11-00/DC"
                                Case "UT55A"
                                    strBase = "UT55A-000-11-00/DC"
                                Case "UP55A"
                                    strBase = "UP55A-000-11-00/DC"
                                Case "UP35A"
                                    strBase = "UP35A-000-11-00/DC"
                                Case "UM33A"
                                    strBase = "UM33A-000-11-00/DC"
                            End Select
                        End If
                    End If
                Case "1"
                    If intTemp = 0 Then
                        If InStr(strModel, "DC") = False Then
                            Select Case strType
                                Case "UT32A"
                                    strBase = "UT32A-100-10-00"
                                Case "UT35A"
                                    strBase = "UT35A-100-10-00"
                                Case "UT52A"
                                    strBase = "UT52A-100-10-00"
                                Case "UT55A"
                                    strBase = "UT55A-100-10-00"
                                Case "UP55A"
                                    strBase = "UP55A-100-10-00"
                                Case "UP35A"
                                    strBase = "UP35A-100-10-00"
                                Case "UM33A"
                                    strBase = "UM33A-100-10-00"
                            End Select
                        Else 'dc true
                            txtMSG.Text = "Confirm /DC base unit is in stock."
                            Select Case strType
                                Case "UT32A"
                                    strBase = "UT32A-100-10-00/DC"
                                Case "UT35A"
                                    strBase = "UT35A-100-10-00/DC"
                                Case "UT52A"
                                    strBase = "UT52A-100-10-00/DC"
                                Case "UT55A"
                                    strBase = "UT55A-100-10-00/DC"
                                Case "UP55A"
                                    strBase = "UP55A-100-10-00/DC"
                                Case "UP35A"
                                    strBase = "UP35A-100-10-00/DC"
                                Case "UM33A"
                                    strBase = "UM33A-100-10-00/DC"
                            End Select
                        End If
                    Else 'intTemp =1
                        If InStr(strModel, "DC") = False Then
                            Select Case strType
                                Case "UT32A"
                                    strBase = "UT32A-100-11-00"
                                Case "UT35A"
                                    strBase = "UT35A-100-11-00"
                                Case "UT52A"
                                    strBase = "UT52A-100-11-00"
                                Case "UT55A"
                                    strBase = "UT55A-100-11-00"
                                Case "UP55A"
                                    strBase = "UP55A-100-11-00"
                                Case "UP35A"
                                    strBase = "UP35A-100-11-00"
                                Case "UM33A"
                                    strBase = "UM33A-100-11-00"
                            End Select
                        Else 'if dc = true
                            txtMSG.Text = "Confirm /DC base unit is in stock."
                            Select Case strType
                                Case "UT32A"
                                    strBase = "UT32A-100-11-00/DC"
                                Case "UT35A"
                                    strBase = "UT35A-100-11-00/DC"
                                Case "UT52A"
                                    strBase = "UT52A-100-11-00/DC"
                                Case "UT55A"
                                    strBase = "UT55A-100-11-00/DC"
                                Case "UP55A"
                                    strBase = "UP55A-100-11-00/DC"
                                Case "UP35A"
                                    strBase = "UP35A-100-11-00/DC"
                                Case "UM33A"
                                    strBase = "UM33A-100-11-00/DC"
                            End Select
                        End If
                    End If
                Case "2"
                    If intTemp = 0 Then
                        If InStr(strModel, "DC") = False Then
                            Select Case strType
                                Case "UT32A"
                                    strBase = "UT32A-200-10-00"
                                Case "UT35A"
                                    strBase = "UT35A-200-10-00"
                                Case "UT52A"
                                    strBase = "UT52A-200-10-00"
                                Case "UT55A"
                                    strBase = "UT55A-200-10-00"
                                Case "UP55A"
                                    strBase = "UP55A-200-10-00"
                                Case "UP35A"
                                    strBase = "UP35A-200-10-00"
                                Case "UM33A"
                                    strBase = "UM33A-200-10-00"
                            End Select
                        Else 'dc true
                            txtMSG.Text = "Confirm /DC base unit is in stock."
                            Select Case strType
                                Case "UT32A"
                                    strBase = "UT32A-200-10-00/DC"
                                Case "UT35A"
                                    strBase = "UT35A-200-10-00/DC"
                                Case "UT52A"
                                    strBase = "UT52A-200-10-00/DC"
                                Case "UT55A"
                                    strBase = "UT55A-200-10-00/DC"
                                Case "UP55A"
                                    strBase = "UP55A-200-10-00/DC"
                                Case "UP35A"
                                    strBase = "UP35A-200-10-00/DC"
                                Case "UM33A"
                                    strBase = "UM33A-200-10-00/DC"
                            End Select
                        End If
                    Else 'intTemp =1
                        If InStr(strModel, "DC") = False Then
                            Select Case strType
                                Case "UT32A"
                                    strBase = "UT32A-200-11-00"
                                Case "UT35A"
                                    strBase = "UT35A-200-11-00"
                                Case "UT52A"
                                    strBase = "UT52A-200-11-00"
                                Case "UT55A"
                                    strBase = "UT55A-200-11-00"
                                Case "UP55A"
                                    strBase = "UP55A-200-11-00"
                                Case "UP35A"
                                    strBase = "UP35A-200-11-00"
                                Case "UM33A"
                                    strBase = "UM33A-200-11-00"
                            End Select
                        Else 'if dc = true
                            txtMSG.Text = "Confirm /DC base unit is in stock."
                            Select Case strType
                                Case "UT32A"
                                    strBase = "UT32A-200-11-00/DC"
                                Case "UT35A"
                                    strBase = "UT35A-200-11-00/DC"
                                Case "UT52A"
                                    strBase = "UT52A-200-11-00/DC"
                                Case "UT55A"
                                    strBase = "UT55A-200-11-00/DC"
                                Case "UP55A"
                                    strBase = "UP55A-200-11-00/DC"
                                Case "UP35A"
                                    strBase = "UP35A-200-11-00/DC"
                                Case "UM33A"
                                    strBase = "UM33A-200-11-00/DC"
                            End Select
                        End If
                    End If
                Case "N" 'DTL orders; COLOR IS FIXED TO N(NONE)
                    If Mid(strModel, 16, 1) <> "/" Then 'NOT MOD
                        If InStr(strModel, "UN", CompareMethod.Text) > 0 And InStr(strModel, "DC", CompareMethod.Text) = 0 Then
                            If intTemp = "0" Then
                                Select Case strType
                                    Case "UT32A"
                                        strBase = "UT32A-000-10-00"
                                    Case "UT35A"
                                        strBase = "UT35A-000-10-00"
                                    Case "UT52A"
                                        strBase = "UT52A-000-10-00"
                                    Case "UT55A"
                                        strBase = "UT55A-000-10-00"
                                    Case "UP55A"
                                        strBase = "UP55A-000-10-00"
                                    Case "UP35A"
                                        strBase = "UP35A-000-10-00"
                                    Case "UM33A"
                                        txtMSG.Text = "Invalid part number."
                                End Select
                            Else 'intTemp = 1
                                Select Case strType
                                    Case "UT32A"
                                        strBase = "UT32A-000-11-00"
                                    Case "UT35A"
                                        strBase = "UT35A-000-11-00"
                                    Case "UT52A"
                                        strBase = "UT52A-000-11-00"
                                    Case "UT55A"
                                        strBase = "UT55A-000-11-00"
                                    Case "UP55A"
                                        strBase = "UP55A-000-11-00"
                                    Case "UP35A"
                                        strBase = "UP35A-000-11-00"
                                    Case "UM33A"
                                        txtMSG.Text = "Invalid part number."
                                End Select
                            End If
                        ElseIf InStr(strModel, "UN", CompareMethod.Text) > 0 And InStr(strModel, "DC", CompareMethod.Text) > 0 = True Then
                            txtMSG.Text = "Please make sure the /DC base unit is in stock"
                            Select Case strType
                                Case "UT32A"
                                    strBase = "UT32A-000-10-00/DC"
                                Case "UT35A"
                                    strBase = "UT35A-000-10-00/DC"
                                Case "UT52A"
                                    strBase = "UT52A-000-10-00/DC"
                                Case "UT55A"
                                    strBase = "UT55A-000-10-00/DC"
                                Case "UP55A"
                                    strBase = "UP55A-000-10-00/DC"
                                Case "UP35A"
                                    strBase = "UP35A-000-10-00/DC"
                                Case "UM33A"
                                    txtMSG.Text = "Invalid part number."
                                    cmdClear.PerformClick()
                                    GoTo EndLine
                            End Select
                        ElseIf InStr(strModel, "UN", CompareMethod.Text) > 0 And intTemp = "1" And InStr(strModel, "DC", CompareMethod.Text) = 0 Then
                            Select Case strType
                                Case "UT32A"
                                    strBase = "UT32A-000-11-00"
                                Case "UT35A"
                                    strBase = "UT35A-000-11-00"
                                Case "UT52A"
                                    strBase = "UT52A-000-11-00"
                                Case "UT55A"
                                    strBase = "UT55A-000-11-00"
                                Case "UP55A"
                                    strBase = "UP55A-000-11-00"
                                Case "UP35A"
                                    strBase = "UP35A-000-11-00"
                                Case "UM33A"
                                    txtMSG.Text = "Invalid part number."
                                    cmdClear.PerformClick()
                                    GoTo EndLine
                            End Select
                        ElseIf InStr(strModel, "UN", CompareMethod.Text) > 0 And intTemp = "1" And InStr(strModel, "DC", CompareMethod.Text) > 0 Then
                            txtMSG.Text = "Confirm /DC base unit is in stock."
                            Select Case strType
                                Case "UT32A"
                                    strBase = "UT32A-000-11-00/DC"
                                Case "UT35A"
                                    strBase = "UT35A-000-11-00/DC"
                                Case "UT52A"
                                    strBase = "UT52A-000-11-00/DC"
                                Case "UT55A"
                                    strBase = "UT55A-000-11-00/DC"
                                Case "UP55A"
                                    strBase = "UP55A-000-11-00/DC"
                                Case "UP35A"
                                    strBase = "UP35A-000-11-00/DC"
                                Case "UM33A"
                                    txtMSG.Text = "Invalid part number."
                                    cmdClear.PerformClick()
                                    GoTo EndLine
                            End Select
                        ElseIf InStr(strModel, "PN", CompareMethod.Text) > 0 And InStr(strModel, "DC", CompareMethod.Text) = 0 Then
                            Select Case strType
                                Case "UT32A"
                                    strBase = "UT32A-100-10-00"
                                Case "UT35A"
                                    strBase = "UT35A-100-10-00"
                                Case "UT52A"
                                    strBase = "UT52A-100-10-00"
                                Case "UT55A"
                                    strBase = "UT55A-100-10-00"
                                Case "UP55A"
                                    strBase = "UP55A-100-10-00"
                                Case "UP35A"
                                    strBase = "UP35A-100-10-00"
                                Case "UM33A"
                                    txtMSG.Text = "Invalid part number."
                                    cmdClear.PerformClick()
                                    GoTo EndLine
                            End Select
                        ElseIf InStr(strModel, "PN", CompareMethod.Text) > 0 And InStr(strModel, "DC", CompareMethod.Text) > 0 Then
                            txtMSG.Text = "Confirm /DC base unit is in stock."
                            Select Case strType
                                Case "UT32A"
                                    strBase = "UT32A-100-10-00/DC"
                                Case "UT35A"
                                    strBase = "UT35A-100-10-00/DC"
                                Case "UT52A"
                                    strBase = "UT52A-100-10-00/DC"
                                Case "UT55A"
                                    strBase = "UT55A-100-10-00/DC"
                                Case "UP55A"
                                    strBase = "UP55A-100-10-00/DC"
                                Case "UP35A"
                                    strBase = "UP35A-100-10-00/DC"
                                Case "UM33A"
                                    txtMSG.Text = "Invalid part number."
                                    cmdClear.PerformClick()
                                    GoTo EndLine
                            End Select
                        ElseIf InStr(strModel, "PN", CompareMethod.Text) > 0 And intTemp = "1" And InStr(strModel, "DC", CompareMethod.Text) = 0 Then
                            Select Case strType
                                Case "UT32A"
                                    strBase = "UT32A-100-11-00"
                                Case "UT35A"
                                    strBase = "UT35A-100-11-00"
                                Case "UT52A"
                                    strBase = "UT52A-100-11-00"
                                Case "UT55A"
                                    strBase = "UT55A-100-11-00"
                                Case "UP55A"
                                    strBase = "UP55A-100-11-00"
                                Case "UP35A"
                                    strBase = "UP35A-100-11-00"
                                Case "UM33A"
                                    txtMSG.Text = "Invalid part number."
                                    cmdClear.PerformClick()
                                    GoTo EndLine
                            End Select
                        ElseIf InStr(strModel, "PN", CompareMethod.Text) > 0 And intTemp = "1" And InStr(strModel, "DC", CompareMethod.Text) > 0 Then
                            txtMSG.Text = "Confirm /DC base unit is in stock."
                            Select Case strType
                                Case "UT32A"
                                    strBase = "UT32A-100-11-00/DC"
                                Case "UT35A"
                                    strBase = "UT35A-100-11-00/DC"
                                Case "UT52A"
                                    strBase = "UT52A-100-11-00/DC"
                                Case "UT55A"
                                    strBase = "UT55A-100-11-00/DC"
                                Case "UP55A"
                                    strBase = "UP55A-100-11-00/DC"
                                Case "UP35A"
                                    strBase = "UP35A-100-11-00/DC"
                                Case "UM33A"
                                    txtMSG.Text = "Invalid part number."
                                    cmdClear.PerformClick()
                                    GoTo EndLine
                            End Select
                        ElseIf InStr(strModel, "UU", CompareMethod.Text) > 0 And InStr(strModel, "DC", CompareMethod.Text) = 0 Then
                            If intTemp = "0" Then
                                Select Case strType
                                    Case "UT32A"
                                        strBase = "UT32A-200-10-00"
                                    Case "UT35A"
                                        strBase = "UT35A-200-10-00"
                                    Case "UT52A"
                                        strBase = "UT52A-200-10-00"
                                    Case "UT55A"
                                        strBase = "UT55A-200-10-00"
                                    Case "UP55A"
                                        strBase = "UP55A-200-10-00"
                                    Case "UP35A"
                                        strBase = "UP35A-200-10-00"
                                    Case "UM33A"
                                        txtMSG.Text = "Invalid part number."
                                        cmdClear.PerformClick()
                                        GoTo EndLine
                                End Select
                            Else 'intTemp = 1 
                                Select Case strType
                                    Case "UT32A"
                                        strBase = "UT32A-200-11-00"
                                    Case "UT35A"
                                        strBase = "UT35A-200-11-00"
                                    Case "UT52A"
                                        strBase = "UT52A-200-11-00"
                                    Case "UT55A"
                                        strBase = "UT55A-200-11-00"
                                    Case "UP55A"
                                        strBase = "UP55A-200-11-00"
                                    Case "UP35A"
                                        strBase = "UP35A-200-11-00"
                                    Case "UM33A"
                                        txtMSG.Text = "Invalid part number."
                                        cmdClear.PerformClick()
                                        GoTo EndLine
                                End Select
                            End If
                        ElseIf InStr(strModel, "UU", CompareMethod.Text) > 0 And InStr(strModel, "DC", CompareMethod.Text) > 0 Then
                            txtMSG.Text = "Confirm /DC base unit is in stock."
                            If intTemp = "0" Then
                                Select Case strType
                                    Case "UT32A"
                                        strBase = "UT32A-200-10-00/DC"
                                    Case "UT35A"
                                        strBase = "UT35A-200-10-00/DC"
                                    Case "UT52A"
                                        strBase = "UT52A-200-10-00/DC"
                                    Case "UT55A"
                                        strBase = "UT55A-200-10-00/DC"
                                    Case "UP55A"
                                        strBase = "UP55A-200-10-00/DC"
                                    Case "UP35A"
                                        strBase = "UP35A-200-10-00/DC"
                                    Case "UM33A"
                                        txtMSG.Text = "Invalid part number."
                                        cmdClear.PerformClick()
                                        GoTo EndLine
                                End Select
                            Else 'intTemp = 1
                                Select Case strType
                                    Case "UT32A"
                                        strBase = "UT32A-200-11-00/DC"
                                    Case "UT35A"
                                        strBase = "UT35A-200-11-00/DC"
                                    Case "UT52A"
                                        strBase = "UT52A-200-11-00/DC"
                                    Case "UT55A"
                                        strBase = "UT55A-200-11-00/DC"
                                    Case "UP55A"
                                        strBase = "UP55A-200-11-00/DC"
                                    Case "UP35A"
                                        strBase = "UP35A-200-11-00/DC"
                                    Case "UM33A"
                                        txtMSG.Text = "Invalid part number."
                                        cmdClear.PerformClick()
                                        GoTo EndLine
                                End Select
                            End If

                        ElseIf InStr(strModel, "UU") And intTemp = "1" And InStr(txtPartNum.Text, "DC", CompareMethod.Text) = 0 Then
                            Select Case strType
                                Case "UT32A"
                                    strBase = "UT32A-200-11-00"
                                Case "UT35A"
                                    strBase = "UT35A-200-11-00"
                                Case "UT52A"
                                    strBase = "UT52A-200-11-00"
                                Case "UT55A"
                                    strBase = "UT55A-200-11-00"
                                Case "UP55A"
                                    strBase = "UP55A-200-11-00"
                                Case "UP35A"
                                    strBase = "UP35A-200-11-00"
                                Case "UM33A"
                                    txtMSG.Text = "Invalid part number."
                                    cmdClear.PerformClick()
                                    GoTo EndLine
                            End Select
                        ElseIf InStr(strModel, "UU") And intTemp = "1" And InStr(strModel, "DC", CompareMethod.Text) > 0 Then
                            txtMSG.Text = "Confirm /DC base unit is in stock."
                            Select Case strType
                                Case "UT32A"
                                    strBase = "UT32A-200-11-00/DC"
                                Case "UT35A"
                                    strBase = "UT35A-200-11-00/DC"
                                Case "UT52A"
                                    strBase = "UT52A-200-11-00/DC"
                                Case "UT55A"
                                    strBase = "UT55A-200-11-00/DC"
                                Case "UP55A"
                                    strBase = "UP55A-200-11-00/DC"
                                Case "UP35A"
                                    strBase = "UP35A-200-11-00/DC"
                                Case "UM33A"
                                    txtMSG.Text = "Invalid part number."
                                    cmdClear.PerformClick()
                                    GoTo EndLine
                            End Select
                        End If
                    Else 'if mod unit
                        If intTemp = "0" Then
                            If InStr(strModel, "UN", CompareMethod.Text) > 0 Then 'TRUE 
                                If InStr(strModel, "DC", CompareMethod.Text) = 0 Then 'FLASE
                                    Select Case strType
                                        Case "UT32A"
                                            strBase = "UT32A-000-10-00/MD"
                                        Case "UT35A"
                                            strBase = "UT35A-000-10-00/MD"
                                        Case "UT52A"
                                            strBase = "UT52A-000-10-00/MD"
                                        Case "UT55A"
                                            strBase = "UT55A-000-10-00/MD"
                                        Case "UP55A"
                                            strBase = "UP55A-000-10-00/MD"
                                        Case "UP35A"
                                            strBase = "UP35A-000-10-00/MD"
                                        Case "UM33A"
                                            txtMSG.Text = "Invalid part number"
                                            cmdClear.PerformClick()
                                            GoTo EndLine
                                    End Select
                                Else 'DC TRUE
                                    txtMSG.Text = "Confirm /DC base unit is in stock."
                                    Select Case strType
                                        Case "UT32A"
                                            strBase = "UT32A-000-10-00/MD/DC"
                                        Case "UT35A"
                                            strBase = "UT35A-000-10-00/MD/DC"
                                        Case "UT52A"
                                            strBase = "UT52A-000-10-00/MD/DC"
                                        Case "UT55A"
                                            strBase = "UT55A-000-10-00/MD/DC"
                                        Case "UP55A"
                                            strBase = "UP55A-000-10-00/MD/DC"
                                        Case "UP35A"
                                            strBase = "UP35A-000-10-00/MD/DC"
                                        Case "UM33A"
                                            txtMSG.Text = "Invalid part number."
                                            cmdClear.PerformClick()
                                            GoTo EndLine
                                    End Select
                                End If
                            End If
                        ElseIf InStr(strModel, "PN", CompareMethod.Text) > 0 Then
                            If InStr(strModel, "DC", CompareMethod.Text) = 0 Then
                                Select Case strType
                                    Case "UT32A"
                                        strBase = "UT32A-100-10-00/MD"
                                    Case "UT35A"
                                        strBase = "UT35A-100-10-00/MD"
                                    Case "UT52A"
                                        strBase = "UT52A-100-10-00/MD"
                                    Case "UT55A"
                                        strBase = "UT55A-100-10-00/MD"
                                    Case "UP55A"
                                        strBase = "UP55A-100-10-00/MD"
                                    Case "UP35A"
                                        strBase = "UP35A-100-10-00/MD"
                                    Case "UM33A"
                                        txtMSG.Text = "Invalid part number."
                                        cmdClear.PerformClick()
                                        GoTo EndLine
                                End Select
                            Else 'DC TRUE
                                txtMSG.Text = "Confirm /DC base unit is in stock."
                                Select Case strType
                                    Case "UT32A"
                                        strBase = "UT32A-100-10-00/MD/DC"
                                    Case "UT35A"
                                        strBase = "UT35A-100-10-00/MD/DC"
                                    Case "UT52A"
                                        strBase = "UT52A-100-10-00/MD/DC"
                                    Case "UT55A"
                                        strBase = "UT55A-100-10-00/MD/DC"
                                    Case "UP55A"
                                        strBase = "UP55A-100-10-00/MD/DC"
                                    Case "UP35A"
                                        strBase = "UP35A-100-10-00/MD/DC"
                                    Case "UM33A"
                                        txtMSG.Text = "Invalid part number."
                                        cmdClear.PerformClick()
                                        GoTo EndLine
                                End Select
                            End If
                        ElseIf InStr(strModel, "UU", CompareMethod.Text) > 0 Then
                            If InStr(strModel, "DC", CompareMethod.Text) = 0 Then
                                Select Case strType
                                    Case "UT32A"
                                        strBase = "UT32A-200-10-00/MD"
                                    Case "UT35A"
                                        strBase = "UT35A-200-10-00/MD"
                                    Case "UT52A"
                                        strBase = "UT52A-200-10-00/MD"
                                    Case "UT55A"
                                        strBase = "UT55A-200-10-00/MD"
                                    Case "UP55A"
                                        strBase = "UP55A-200-10-00/MD"
                                    Case "UP35A"
                                        strBase = "UP35A-200-10-00/MD"
                                    Case "UM33A"
                                        txtMSG.Text = "Invalid part number."
                                        cmdClear.PerformClick()
                                        GoTo EndLine
                                End Select
                            Else  'DC TRUE
                                txtMSG.Text = "Confirm /DC base unit is in stock."
                                Select Case strType
                                    Case "UT32A"
                                        strBase = "UT32A-200-10-00/MD/DC"
                                    Case "UT35A"
                                        strBase = "UT35A-200-10-00/MD/DC"
                                    Case "UT52A"
                                        strBase = "UT52A-200-10-00/MD/DC"
                                    Case "UT55A"
                                        strBase = "UT55A-200-10-00/MD/DC"
                                    Case "UP55A"
                                        strBase = "UP55A-200-10-00/MD/DC"
                                    Case "UP35A"
                                        strBase = "UP35A-200-10-00/MD/DC"
                                    Case "UM33A"
                                        txtMSG.Text = "Invalid part number."
                                        cmdClear.PerformClick()
                                        GoTo EndLine
                                End Select
                            End If
                        ElseIf Mid(strModel, 12, 1) = "1" Then
                            If InStr(strModel, "UN", CompareMethod.Text) > 0 Then
                                If InStr(strModel, "DC", CompareMethod.Text) > 0 Then
                                    txtMSG.Text = "Confirm /DC base unit is in stock."
                                    Select Case strType
                                        Case "UT32A"
                                            strBase = "UT32A-000-11-00/MD/DC"
                                        Case "UT35A"
                                            strBase = "UT35A-000-11-00/MD/DC"
                                        Case "UT52A"
                                            strBase = "UT52A-000-11-00/MD/DC"
                                        Case "UT55A"
                                            strBase = "UT55A-000-11-00/MD/DC"
                                        Case "UP55A"
                                            strBase = "UP55A-000-11-00/MD/DC"
                                        Case "UP35A"
                                            strBase = "UP35A-000-11-00/MD/DC"
                                        Case "UM33A"
                                            txtMSG.Text = "Invalid part number."
                                            cmdClear.PerformClick()
                                            GoTo EndLine
                                    End Select
                                Else
                                    Select Case strType
                                        Case "UT32A"
                                            strBase = "UT32A-000-11-00/MD"
                                        Case "UT35A"
                                            strBase = "UT35A-000-11-00/MD"
                                        Case "UT52A"
                                            strBase = "UT52A-000-11-00/MD"
                                        Case "UT55A"
                                            strBase = "UT55A-000-11-00/MD"
                                        Case "UP55A"
                                            strBase = "UP55A-000-11-00/MD"
                                        Case "UP35A"
                                            strBase = "UP35A-000-11-00/MD"
                                        Case "UM33A"
                                            txtMSG.Text = "Invalid part number."
                                            cmdClear.PerformClick()
                                            GoTo EndLine
                                    End Select
                                End If
                            ElseIf InStr(strModel, "PN", CompareMethod.Text) > 0 Then
                                If InStr(strModel, "DC", CompareMethod.Text) = 0 Then
                                    Select Case strType
                                        Case "UT32A"
                                            strBase = "UT32A-100-11-00/MD"
                                        Case "UT35A"
                                            strBase = "UT35A-100-11-00/MD"
                                        Case "UT52A"
                                            strBase = "UT52A-100-11-00/MD"
                                        Case "UT55A"
                                            strBase = "UT55A-100-11-00/MD"
                                        Case "UP55A"
                                            strBase = "UP55A-100-11-00/MD"
                                        Case "UP35A"
                                            strBase = "UP35A-100-11-00/MD"
                                        Case "UM33A"
                                            txtMSG.Text = "Invalid part number."
                                            cmdClear.PerformClick()
                                            GoTo EndLine
                                    End Select
                                Else  'DC TRUE
                                    txtMSG.Text = "Confirm /DC base unit is in stock."
                                    Select Case strType
                                        Case "UT32A"
                                            strBase = "UT32A-100-11-00/MD/DC"
                                        Case "UT35A"
                                            strBase = "UT35A-100-11-00/MD/DC"
                                        Case "UT52A"
                                            strBase = "UT52A-100-11-00/MD/DC"
                                        Case "UT55A"
                                            strBase = "UT55A-100-11-00/MD/DC"
                                        Case "UP55A"
                                            strBase = "UP55A-100-11-00/MD/DC"
                                        Case "UP35A"
                                            strBase = "UP35A-100-11-00/MD/DC"
                                        Case "UM33A"
                                            txtMSG.Text = "Invalid part number."
                                            cmdClear.PerformClick()
                                            GoTo EndLine
                                    End Select
                                End If
                            ElseIf InStr(strModel, "UU", CompareMethod.Text) > 0 Then
                                txtMSG.Text = "Confirm /DC base unit is in stock."
                                If InStr(strModel, "DC", CompareMethod.Text) = 0 Then
                                    Select Case strType
                                        Case "UT32A"
                                            strBase = "UT32A-200-11-00/MD"
                                        Case "UT35A"
                                            strBase = "UT35A-200-11-00/MD"
                                        Case "UT52A"
                                            strBase = "UT52A-200-11-00/MD"
                                        Case "UT55A"
                                            strBase = "UT55A-200-11-00/MD"
                                        Case "UP55A"
                                            strBase = "UP55A-200-11-00/MD"
                                        Case "UP35A"
                                            strBase = "UP35A-200-11-00/MD"
                                        Case "UM33A"
                                            txtMSG.Text = "Invalid part number."
                                            cmdClear.PerformClick()
                                            GoTo EndLine
                                    End Select
                                Else 'DC = TRUE
                                    txtMSG.Text = "Confirm /DC base unit is in stock."
                                    Select Case strType
                                        Case "UT32A"
                                            strBase = "UT32A-200-11-00/MD/DC"
                                        Case "UT35A"
                                            strBase = "UT35A-200-11-00/MD/DC"
                                        Case "UT52A"
                                            strBase = "UT52A-200-11-00/MD/DC"
                                        Case "UT55A"
                                            strBase = "UT55A-200-11-00/MD/DC"
                                        Case "UP55A"
                                            strBase = "UP55A-200-11-00/MD/DC"
                                        Case "UP35A"
                                            strBase = "UP35A-200-11-00/MD/DC"
                                        Case "UM33A"
                                            txtMSG.Text = "Invalid part number."
                                            cmdClear.PerformClick()
                                            GoTo EndLine
                                    End Select
                                End If
                            End If
                        End If
                    End If
                Case Else
                    cmdClear.PerformClick()
                    GoTo EndLine
            End Select

            If Not InStr(strBase, "/MD", CompareMethod.Text) Then
                strBase = strBase & "/MD"
            End If

            txtBaseMod.Text = strBase

            ' not sure if I need this
            If InStr(strModel, "/MD", CompareMethod.Text) > 0 Then
                Select Case strModel
                    Case "UT55A", "UP55A"
                        myMainAssy = strModel & "/E1/E2/E3/E4/S1"
                    Case "UT52A", "UT32A"
                        myMainAssy = strModel & "/E1/S1"
                    Case "UT35A", "UP35A"
                        myMainAssy = strModel & "/E1/E3/E4/S1"
                    Case "UM33A"
                        myMainAssy = strModel & "/E1"
                End Select
            End If

            Call DeviceMSCode(strModel)

        End If

EndLine:

    End Sub

    Private Sub Label3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Label4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label4.Click

    End Sub

    Private Sub Label2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label2.Click

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub txtMsg_TextChanged(sender As Object, e As EventArgs)

    End Sub
End Class
