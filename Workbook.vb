﻿Option Explicit On
Option Strict Off

Imports Microsoft.Office.Interop.Excel
Imports System.Drawing
Imports System.Collections.ObjectModel

Public Class Workbook
    Private Const ERROR_NO_ERRORS As String = "No Error"

    Public oApp As Microsoft.Office.Interop.Excel.Application
    Public oSheet() As Microsoft.Office.Interop.Excel.Worksheet    'One oSheet for each page in workbook.

    '-- Used in killing EXCEL processes
    Private priorSum As Integer = 0
    Private newSum As Integer = 0
    Private xlProcID As Integer = 0

    Private _IsVisible As Boolean

    Private _CurrentSheet As Integer

    'Remember where last saved file was put

#Region "Properties"

    Public Property TempFileLoc As String

    Public Property LastError As String

    Public Property IsReady As Boolean

    ''' <summary>
    ''' Master template file location as set by Script
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Master_File As String

    ''' <summary>
    ''' Gets the current file name (without full path) of loaded file.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property Name As String
        Get
            If Master_File.Contains("\") Then
                Return Master_File.Substring(Master_File.LastIndexOf("\") + 1)
            Else
                Return "Unknown"
            End If
        End Get
    End Property

    ''' <summary>
    ''' Returns the PATH portion of the masterfile location
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property Path As String
        Get
            If Master_File.Contains("\") Then
                Return Master_File.Substring(0, Master_File.LastIndexOf("\")) & "\"
            Else
                Return String.Empty
            End If
        End Get
    End Property

    Public ReadOnly Property CurrentTabName As String
        Get
            If Master_File <> "" Then
                Return oSheet(_CurrentSheet).Name
            Else
                Return "Unknown"
            End If
        End Get
    End Property

    ''' <summary>
    ''' Gets/Sets the currently active worksheet. Note that the sheets are 1 based.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CurrentSheet As Integer
        Get
            Return _CurrentSheet
        End Get
        Set(value As Integer)
            _CurrentSheet = value
            Switch_To_Sheet(_CurrentSheet)
        End Set
    End Property

    ''' <summary>
    ''' Toggles the visibility of the sheet
    ''' </summary>
    ''' <returns></returns>
    Public Property Visible As Boolean              'Show worksheet while in progress
        Get
            Return _IsVisible
        End Get
        Set(value As Boolean)
            _IsVisible = value

            If Not IsNothing(oApp) Then
                oApp.Visible = _IsVisible
                oApp.ScreenUpdating = _IsVisible
                'If _IsVisible = True Then
                '    oApp.ScreenUpdating = True
                'End If
            End If

        End Set
    End Property

    ''' <summary>
    ''' Set to FALSE to pause screen updating
    ''' to enable faster writing to the sheet
    ''' </summary>
    ''' <returns></returns>
    Public Property ScreenUpdating As Boolean
        Get
            Return oApp.ScreenUpdating
        End Get
        Set(value As Boolean)
            oApp.ScreenUpdating = value
        End Set
    End Property

    Public Property SaveFileLocation As String

#End Region

    '-----------------------------------------
    ' Get ready for a Run.
    ' 1) Setup Excel object
    ' 2) Get and copy master file
    ' 3) Set visible or no
    '-----------------------------------------

    Public Sub Reset()

        xlProcID = 0
        newSum = 0
        priorSum = 0
        LastError = Nothing

    End Sub

    Public Function Initialize() As Boolean

        '------------
        ' Check if master file exists
        ' Create local copy
        ' Setup for removing the Excel instance later
        ' Open the workbook, then set up a collection of sheet objects, each equal to a sheet
        ' in the workbook.
        '------------

        Try

            'KillAllExcelInstances()

            Reset()

            Dim fileExists As Boolean
            fileExists = My.Computer.FileSystem.FileExists(Master_File)

            If fileExists = False Then
                LastError = "Master file not found at: " & Master_File
                Return False
            End If

            '1) Copy master template over locally
            'For i As Integer = 1 To 100
            _TempFileLoc = System.IO.Path.GetTempFileName

            '    Dim fileExists1 As Boolean
            '    fileExists1 = My.Computer.FileSystem.FileExists(_TempFileLoc)

            '    If fileExists1 = False Then Exit For

            'Next

            _TempFileLoc = _TempFileLoc.Substring(0, _TempFileLoc.IndexOf("."))

            If Master_File.EndsWith(".xlsm") Then
                _TempFileLoc += ".xlsm"
            Else
                _TempFileLoc += ".xlsx"
            End If

            IO.File.Copy(Master_File, _TempFileLoc, True)

            '2) Get listing of EXCEL processes which may already be running; let them live.
            For Each proc As Process In Process.GetProcessesByName("EXCEL")
                priorSum += proc.Id
            Next proc

            '3) Instantiate a new instance of EXCEL; create an array of oSheets
            oApp = New Microsoft.Office.Interop.Excel.Application

            oApp.Workbooks.Open(_TempFileLoc)

            ReDim oSheet(oApp.Worksheets.Count)
            For i As Integer = 1 To oApp.Worksheets.Count
                oSheet(i) = CType(oApp.Worksheets(i), Microsoft.Office.Interop.Excel.Worksheet)
            Next

            oSheet(1).Activate()    'Default to first tab

            oApp.ScreenUpdating = Visible
            oApp.Visible = Visible
            oApp.WindowState = Microsoft.Office.Interop.Excel.XlWindowState.xlMinimized

            'Determine the 'newSum' of the Existing "Excel" Processs:
            For Each proc As Process In Process.GetProcessesByName("excel")
                newSum += proc.Id
            Next proc

            '4) Subtract the difference to determine the 'xlProcID'
            xlProcID = newSum - priorSum

            IsReady = True
            Return True
        Catch ex As Exception
            LastError = ex.ToString
            'Write_Log(ex.ToString)
            Try
                Kill_It()
            Catch
            End Try
            IsReady = False
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Copies a worksheet up from another document
    ''' and sets it as current sheet
    ''' </summary>
    ''' <param name="foreignWorkbookPath">Fully qualified path to the source excel document</param>
    ''' <param name="foreignSheetName">Name of the sheet we want to copy out</param>
    ''' <param name="newSheetName">Name the sheet will be called in host document</param>
    ''' <returns></returns>
    Public Function CopyForiegnWorkSheet(foreignWorkbookPath As String,
                                         foreignSheetName As String,
                                         newSheetName As String) As Boolean
        Try

            Dim book1 As Microsoft.Office.Interop.Excel.Workbook
            Dim book2 As Microsoft.Office.Interop.Excel.Workbook

            book1 = oApp.Workbooks(1)                           'HOST
            book2 = oApp.Workbooks.Open(foreignWorkbookPath)    'FOREIGN SOURCE

            'Normally a sheet would already have been chosen by now
            'but in case it hasn't default to the first sheet
            If CurrentSheet = 0 Then CurrentSheet = 1

            'Copy to the sheet after current one
            book2.Worksheets(foreignSheetName).copy(After:=book1.Sheets(CurrentSheet))

            book2.Saved = True
            book2.Close()

            'Rebuild array of sheets
            ReDim oSheet(oApp.Worksheets.Count)
            For i As Integer = 1 To oApp.Worksheets.Count
                oSheet(i) = CType(oApp.Worksheets(i), Microsoft.Office.Interop.Excel.Worksheet)
            Next

            Switch_To_Sheet(foreignSheetName)

            Dim excelWorkSheet As Microsoft.Office.Interop.Excel.Worksheet
            excelWorkSheet = book1.Sheets(CurrentSheet)
            excelWorkSheet.Name = newSheetName

            Return True

        Catch ex As Exception
            LastError = ex.Message
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Initializes instance with pre-defined file (TROUBLESHOOTING USE)
    ''' </summary>
    ''' <param name="fileName"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Initialize(fileName As String) As Boolean

        '------------
        ' Check if master file exists
        ' Create local copy
        ' Setup for removing the Excel instance later
        ' Open the workbook, then set up a collection of sheet objects, each equal to a sheet
        ' in the workbook.
        '------------

        Try

            Reset()

            'Dim fileExists As Boolean
            'fileExists = My.Computer.FileSystem.FileExists(Master_File)

            'If fileExists = False Then
            '    LastError = "Master file not found at: " & Master_File
            '    Return False
            'End If

            ''1) Copy master template over locally
            '_TempFileLoc = System.IO.Path.GetTempFileName
            '_TempFileLoc = _TempFileLoc.Substring(0, _TempFileLoc.IndexOf("."))
            '_TempFileLoc += ".xlsx"

            'System.IO.File.Copy(Master_File, _TempFileLoc, True)

            '2) Get listing of EXCEL processes which may already be running; let them live.
            For Each proc As Process In Process.GetProcessesByName("EXCEL")
                priorSum += proc.Id
            Next proc

            '3) Instantiate a new instance of EXCEL; create an array of oSheets
            oApp = New Microsoft.Office.Interop.Excel.Application

            oApp.Workbooks.Open(fileName)

            ReDim oSheet(oApp.Worksheets.Count)
            For i As Integer = 1 To oApp.Worksheets.Count
                oSheet(i) = CType(oApp.Worksheets(i), Microsoft.Office.Interop.Excel.Worksheet)
            Next

            oSheet(1).Activate()    'Default to first tab

            oApp.ScreenUpdating = Visible
            oApp.Visible = Visible
            oApp.WindowState = Microsoft.Office.Interop.Excel.XlWindowState.xlMinimized

            'Determine the 'newSum' of the Existing "Excel" Processs:
            For Each proc As Process In Process.GetProcessesByName("excel")
                newSum += proc.Id
            Next proc

            '4) Subtract the difference to determine the 'xlProcID'
            xlProcID = newSum - priorSum

            IsReady = True
            Return True
        Catch ex As Exception
            LastError = ex.ToString
            'Write_Log(ex.ToString)
            Try
                Kill_It()
            Catch
            End Try
            IsReady = False
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Makes the temporary xls file in use visible and front-and-center
    ''' </summary>
    Public Sub Maximize()
        Try
            If oApp IsNot Nothing Then
                oApp.ScreenUpdating = True
                oApp.Visible = True
                oApp.WindowState = XlWindowState.xlNormal
            End If
        Catch
        End Try
    End Sub

    ''' <summary>
    ''' writes the current temporary file to user's MyDocuments folder
    ''' and launches it under a separate handler.
    ''' </summary>
    Public Sub SaveAndLaunch()
        Try
            If oApp IsNot Nothing Then

                'Default save location
                Dim defaultSaveLoc As String = My.Computer.FileSystem.SpecialDirectories.MyDocuments & "\" & Name

                'Get saveas dialog
                Dim nFileLoc As String = oApp.GetSaveAsFilename(defaultSaveLoc)

                'Abort if nothing chosen
                If nFileLoc = "" Then Return

                'Add extension if absent
                If Not nFileLoc.EndsWith(".xlsx") Then
                    If nFileLoc.EndsWith(".") Then
                        nFileLoc = nFileLoc.Substring(0, nFileLoc.Length - 1)
                    End If
                    nFileLoc = nFileLoc & ".xlsx"
                End If

                'Save to that location then quit
                oApp.ActiveWorkbook.SaveCopyAs(nFileLoc)

                oApp.Quit()

                'Remove all extraneous instances of Excel
                KillAllExcelInstances()

                Threading.Thread.Sleep(1000)

                'Launch in new Excel process
                Process.Start(nFileLoc)

            End If
        Catch
        End Try
    End Sub

    '''' <summary>
    '''' writes the current temporary file to user's MyDocuments folder
    '''' and launches it under a separate handler.
    '''' </summary>
    'Public Sub SaveAndLaunch(FileName As String)
    '    Try
    '        If oApp IsNot Nothing Then

    '            ''Default save location
    '            'Dim defaultSaveLoc As String = My.Computer.FileSystem.SpecialDirectories.MyDocuments & "\" & Name

    '            ''Get saveas dialog
    '            'Dim nFileLoc As String = oApp.GetSaveAsFilename(defaultSaveLoc)

    '            ''Abort if nothing chosen
    '            'If nFileLoc = "" Then Return

    '            ''Add extension if absent
    '            'If Not nFileLoc.EndsWith(".xlsx") Then
    '            '    If nFileLoc.EndsWith(".") Then
    '            '        nFileLoc = nFileLoc.Substring(0, nFileLoc.Length - 1)
    '            '    End If
    '            '    nFileLoc = nFileLoc & ".xlsx"
    '            'End If

    '            'Save to that location
    '            oApp.ActiveWorkbook.SaveCopyAs(FileName)

    '            'Kill_It()

    '            'Launch
    '            Process.Start(FileName)


    '        End If
    '    Catch
    '    End Try
    'End Sub

    ''' <summary>
    ''' If the final workbook has been created, invoking this routine
    ''' will open that file and close/delete the temp. file. Call this
    ''' at the end of the test; never during.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Launch_Final_WorkSheet()

        Try
            Dim fileExists As Boolean
            fileExists = My.Computer.FileSystem.FileExists(SaveFileLocation)

            If fileExists = True Then

                'Remove temp file
                Kill_It()

                'Launch excel process using default handler
                Process.Start(SaveFileLocation)

            End If
        Catch ex As Exception
            MsgBox("Unable to display final Excel sheet", "", MsgBoxStyle.Exclamation)
        End Try

    End Sub

    ''' <summary>
    ''' Attempt to release/kill the EXCEL object using a variety of methods.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Kill_It()
        'Excel almost never exits gracefully; just kill the process and be done with it.

        Try

            If IsNothing(oApp) Then
                Return
            End If

            'Method #1: Gracefully exit app
            Try
                oApp.Visible = False
                oApp.ScreenUpdating = False
                If oApp.ActiveWorkbook Is Nothing Then Return
                oApp.ActiveWorkbook.Saved = True
                oApp.Quit()
                oApp = Nothing
            Catch
            End Try

            'Method #2: Nuke the temp file from orbit; kill EXCEL process
            If xlProcID <> 0 Then
                'oApp.Quit()                    'Rarely works as advertised; just nuke it.
                Dim proc As Process = Process.GetProcessById(xlProcID)
                proc.Kill()
                'Try                             'Remove temp file
                '    My.Computer.FileSystem.DeleteFile(_TempFileLoc, FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently)
                'Catch
                'End Try
            End If

            ''Release the object
            'Try
            '    For j As Integer = 0 To 64
            '        If System.Runtime.InteropServices.Marshal.ReleaseComObject(oApp) = 0 Then
            '            Exit For
            '        Else
            '            DoEvents()
            '        End If
            '    Next
            'Catch
            'End Try

            'Method #3: Physically find and kill the temp file.
            Dim fileExists As Boolean
            fileExists = My.Computer.FileSystem.FileExists(_TempFileLoc)

            If fileExists = True Then
                Try                             'Remove temp file
                    My.Computer.FileSystem.DeleteFile(_TempFileLoc, FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently)
                Catch
                End Try
            End If

        Catch

        Finally
            ReDim oSheet(0)                 'Destroy the array
            oSheet = Nothing                'Release
            oApp = Nothing                  'Release
        End Try

    End Sub

    Public Sub Kill_It_Softly()

        Try

            If IsNothing(oApp) Then
                Return
            End If

            'Method #1: Gracefully exit app
            Try
                oApp.Visible = False
                oApp.ScreenUpdating = False
                oApp.ActiveWorkbook.Saved = True
                oApp.Quit()
                oApp = Nothing
            Catch
            End Try

            ''Method #2: Nuke the temp file from orbit; kill EXCEL process
            'If xlProcID <> 0 Then
            '    'oApp.Quit()                    'Rarely works as advertised; just nuke it.
            '    Dim proc As Process = Process.GetProcessById(xlProcID)
            '    proc.Kill()
            'End If

        Catch

        Finally
            ReDim oSheet(0)                 'Destroy the array
            oSheet = Nothing                'Release
            oApp = Nothing                  'Release
        End Try

    End Sub

    ''' <summary>
    ''' Removes all 'tmpxxxx.pdf' files from temp folder.
    ''' Suggest you run this on startup.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ClearTempFiles()
        Try
            'Get base path
            _TempFileLoc = IO.Path.GetTempPath

            Dim files As ReadOnlyCollection(Of String)
            files = My.Computer.FileSystem.GetFiles(_TempFileLoc, FileIO.SearchOption.SearchAllSubDirectories, "tmp????.pdf")

            'Nuke PDF's
            Parallel.ForEach(files, Sub(file)
                                        Try
                                            My.Computer.FileSystem.DeleteFile(file, FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently)
                                        Catch ex As Exception
                                            'Probably in-use; ignore
                                        End Try
                                    End Sub)

            'Nuke XLSX's
            files = My.Computer.FileSystem.GetFiles(_TempFileLoc, FileIO.SearchOption.SearchAllSubDirectories, "tmp????.xlsx")
            Parallel.ForEach(files, Sub(file)
                                        Try
                                            My.Computer.FileSystem.DeleteFile(file, FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently)
                                        Catch ex As Exception
                                            'Probably in-use; ignore
                                        End Try
                                    End Sub)

            ''Try to also clear out user's TMP files, though they may be more likely to be inuse
            'files = My.Computer.FileSystem.GetFiles(_TempFileLoc, FileIO.SearchOption.SearchAllSubDirectories, "*.tmp")
            'Parallel.ForEach(files, Sub(file)
            '                            Try
            '                                My.Computer.FileSystem.DeleteFile(file, FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently)
            '                            Catch ex As Exception
            '                                'Probably in-use; ignore
            '                            End Try
            '                        End Sub)

            'For Each file As String In files
            '    Try
            '        My.Computer.FileSystem.DeleteFile(file, FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently)
            '    Catch ex As Exception
            '    End Try
            'Next
        Catch ex As Exception

        End Try
    End Sub

    ''' <summary>
    ''' Used mainly for writing to an Excel sheet's textbox or other 'shape'
    ''' </summary>
    ''' <param name="wBox">Name of the shape</param>
    ''' <param name="wValue">Value to print in it</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function WriteShapeText(wBox As String, wValue As String) As Boolean

        Try

            oSheet(CurrentSheet).OLEObjects(wBox).object.text = wValue

            Return True
        Catch ex As Exception
            LastError = ex.Message
            Return False
        End Try

    End Function

    Public Function WriteCell(WhichCELL As String, WhichVALUE As String) As Boolean
        Try
            Dim iPg As Integer

            If WhichCELL.Contains(".") Then
                iPg = CInt(WhichCELL.Substring(1, 1))
                'Strip off the page designator
                WhichCELL = WhichCELL.Substring(WhichCELL.IndexOf(".") + 1)
            Else
                iPg = CurrentSheet
            End If

            oSheet(iPg).Range(WhichCELL).Value = WhichVALUE

            Return True
        Catch ex As Exception
            LastError = ex.ToString
            Return False
        End Try
    End Function

    Public Function WriteCell(Column As String, Row As Integer, WhichVALUE As String) As Boolean
        Try
            Dim iPg As Integer

            Dim whichcell As String = Column & Row.ToString

            If whichcell.Contains(".") Then
                iPg = CInt(whichcell.Substring(1, 1))
                'Strip off the page designator
                whichcell = whichcell.Substring(whichcell.IndexOf(".") + 1)
            Else
                iPg = CurrentSheet
            End If

            'If IsDate(WhichVALUE) Then WhichVALUE = "'" & WhichVALUE


            oSheet(iPg).Range(whichcell).Value = WhichVALUE



            Return True
        Catch ex As Exception
            LastError = ex.ToString
            Return False
        End Try
    End Function

    Public Function WriteDateCell(Column As String, Row As Integer, WhichVALUE As String) As Boolean
        Try
            Dim iPg As Integer

            Dim whichcell As String = Column & Row.ToString

            If whichcell.Contains(".") Then
                iPg = CInt(whichcell.Substring(1, 1))
                'Strip off the page designator
                whichcell = whichcell.Substring(whichcell.IndexOf(".") + 1)
            Else
                iPg = CurrentSheet
            End If

            oApp.Calculate()

            'If IsDate(WhichVALUE) Then WhichVALUE = "'" & WhichVALUE

            oSheet(iPg).Range(whichcell).Value = WhichVALUE

            Return True
        Catch ex As Exception
            LastError = ex.ToString

            Return False
        End Try
    End Function

    ''' <summary>
    ''' Increments ROW when used
    ''' </summary>
    ''' <param name="Column">Letter column</param>
    ''' <param name="Row">numerical row</param>
    ''' <param name="WhichVALUE"></param>
    ''' <returns></returns>
    Public Function WriteCell(Column As String, ByRef Row As Integer, WhichVALUE As String, IncrementRowAfter As Boolean) As Boolean
        Try
            Dim iPg As Integer

            Dim whichcell As String = Column & Row.ToString

            If whichcell.Contains(".") Then
                iPg = CInt(whichcell.Substring(1, 1))
                'Strip off the page designator
                whichcell = whichcell.Substring(whichcell.IndexOf(".") + 1)
            Else
                iPg = CurrentSheet
            End If

            oSheet(iPg).Range(whichcell).Value = WhichVALUE

            Row += 1

            Return True
        Catch ex As Exception
            LastError = ex.ToString
            Return False
        End Try
    End Function

    Public Function ReadCell(WhichCELL As String) As String
        Try

            'WhichCELL should be in format "P2.B2" for consistency sake

            Dim iPg As Integer

            If WhichCELL.Contains(".") Then
                iPg = CInt(WhichCELL.Substring(1, 1))
                'Strip off the page designator
                WhichCELL = WhichCELL.Substring(WhichCELL.IndexOf(".") + 1)
            Else
                iPg = CurrentSheet
            End If

            Return CType(oSheet(iPg).Range(WhichCELL).Value, String)

        Catch ex As Exception
            LastError = ex.ToString
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Reads specified cell, and then advances the Column
    ''' which is passed by reference.
    ''' </summary>
    ''' <param name="Column">Letter of column like 'A' or 'UT' etc</param>
    ''' <param name="Row">Integer value of row number</param>
    ''' <returns></returns>
    Public Function ReadNextCell(ByRef Column As String, Row As Integer) As String
        Try

            ReadNextCell = ReadCell(Column & Row.ToString)

            Column = IncrementColumn(Column)

            Return ReadNextCell

        Catch ex As Exception
            LastError = ex.ToString
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' GET interior cell color
    ''' </summary>
    ''' <param name="WhichCELL"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ReadCellColor(WhichCELL As String) As Color

        Try
            Dim iPg As Integer

            If WhichCELL.Contains(".") Then
                iPg = CInt(WhichCELL.Substring(1, 1))
                'Strip off the page designator
                WhichCELL = WhichCELL.Substring(WhichCELL.IndexOf(".") + 1)
            Else
                iPg = CurrentSheet
            End If

            Dim ColorObj As Color = ColorTranslator.FromWin32(CInt(oSheet(iPg).Range(WhichCELL).Cells.Interior.Color))

            Return ColorObj

        Catch ex As Exception
            LastError = ex.ToString
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Switch currently loaded Excel sheet to specified tab (by number)
    ''' </summary>
    ''' <param name="WhichSheet">Integer starting at 1</param>
    ''' <remarks></remarks>
    Public Overridable Sub Switch_To_Sheet(WhichSheet As Integer)
        If Not IsNothing(oSheet) Then
            oSheet(WhichSheet).Activate()
            _CurrentSheet = WhichSheet
        End If
    End Sub

    ''' <summary>
    ''' Switch currently loaded Excel sheet to specified tab (by sheet title)
    ''' </summary>
    ''' <param name="WhichSheet">Exact name of sheet</param>
    ''' <remarks></remarks>
    Public Overridable Sub Switch_To_Sheet(WhichSheet As String)

        For i As Integer = 1 To oSheet.GetUpperBound(0)
            If oSheet(i).Name.ToUpper.Contains(WhichSheet.ToUpper) Then
                _CurrentSheet = i
                oSheet(i).Activate()
                LastError = ERROR_NO_ERRORS
                Return
            End If

        Next

        _CurrentSheet = 0
        LastError = WhichSheet & " Not Found in workbook: " & Master_File

    End Sub

    Public Sub Calc()
        oApp.Calculate()
    End Sub

    ''' <summary>
    ''' Saves the currently active sheet as a PDF,
    ''' and optionally launches it. If no location given
    ''' to where to save it, saves to temporary folder.
    ''' </summary>
    ''' <param name="andLaunchIt">True: Launches it using default PDF viewer</param>
    ''' <param name="toWhere">Save location and file name to use (if left empty, the temp directory is used)</param>
    ''' <returns>True: Success False: Failure</returns>
    ''' <remarks></remarks>
    Public Function SaveAsPDF(andLaunchIt As Boolean,
                          Optional toWhere As String = Nothing) As Boolean
        Try

            Dim format As Microsoft.Office.Interop.Excel.XlFixedFormatType
            format = XlFixedFormatType.xlTypePDF

            Dim loc As String

            If toWhere = Nothing Then
                loc = TempFileLoc
                'Change extention
                loc = loc.Substring(0, loc.LastIndexOf("."))
                loc = loc & ".pdf"

            Else
                loc = toWhere   'You must provide it a valid path and file name
            End If

            If CurrentSheet <> 0 Then
                oSheet(CurrentSheet).ExportAsFixedFormat(Type:=XlFixedFormatType.xlTypePDF, Filename:=loc,
                                         Quality:=XlFixedFormatQuality.xlQualityStandard,
                                         IncludeDocProperties:=True, IgnorePrintAreas:=False,
                                         OpenAfterPublish:=andLaunchIt)

                Return True
            Else
                LastError = "Invalid sheet selection"
                Return False
            End If

        Catch ex As Exception
            LastError = ex.Message
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Prints the currently-selected sheet; forces to fit 1 page
    ''' </summary>
    ''' <param name="NumCopies"></param>
    ''' <returns>TRUE: Successfully sent to printer FALSE: run-time error</returns>
    ''' <remarks></remarks>
    Public Function PrintJob(Optional NumCopies As Integer = 1) As Boolean
        Try
            oSheet(CurrentSheet).PageSetup.FitToPagesTall = 1
            oSheet(CurrentSheet).PageSetup.FitToPagesWide = 1
            oSheet(CurrentSheet).PrintOutEx(Copies:=NumCopies, Collate:=True)
            'oSheet(CurrentSheet).PageSetup.Zoom = False
            'oApp.ActiveWindow.SelectedSheets.PrintOutEx(Copies:=1, Collate:=True)
            Return True
        Catch ex As Exception
            'Write_Log(ex.ToString)
            Return False
        End Try
    End Function

    'Public Function PrintDocument(wSheet As String, usePDF As Boolean, numCopies As Integer) As Boolean
    '    Try

    '        '-- Activate and print the sheet
    '        Switch_To_Sheet(wSheet)

    '        If usePDF Then
    '            SaveAsPDF(True)
    '        Else
    '            PrintJob(numCopies)
    '        End If

    '        Kill_It_Softly()

    '        Return True
    '    Catch ex As Exception
    '        HandleError(ex)
    '        Return False
    '    End Try
    'End Function

    'Public Function PrintDocument(wSheet As String) As Boolean
    '    Try

    '        '-- Activate and print the sheet
    '        Switch_To_Sheet(wSheet)

    '        Visible = True
    '        ScreenUpdating = True

    '        Return True
    '    Catch ex As Exception
    '        HandleError(ex)
    '        Return False
    '    End Try
    'End Function
    ''' <summary>
    ''' Increments column in a fashion acceptable to Excel
    ''' </summary>
    ''' <param name="wColumn"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function IncrementColumn(wColumn As String) As String

        'Excel is formatted like: A - Z then AA - AZ, then BA - BZ, etc.
        Dim L As Integer, i As Integer, c As Integer
        Dim S As String
        S = wColumn
        L = Len(S)

        Dim retStr As String = ""

        For i = L To 1 Step -1    'go thru the string, right to left
            c = Asc(Mid(S, i, 1)) 'ASCII code of the i-th character
            Select Case c
                Case 65 To 89, 97 To 121  'A-Y or a-y
                    S = Left(S, i - 1) & Chr(c + 1) & Mid(S, i + 1)
                    Exit For
                Case 90    'Z
                    S = Left(S, i - 1) & "A" & Mid(S, i + 1)
                Case 122   'z
                    S = Left(S, i - 1) & "a" & Mid(S, i + 1)
            End Select
            'in the last two cases, we need to continue the loop:
        Next i
        If i = 0 Then
            retStr = retStr.PadRight(L + 1, Chr(65))
            'IncrementColumn = IncrementColumn.PadRight(L + 1, Chr(65))
        Else
            retStr = S
        End If

        Return retStr

    End Function

    Public Sub KillAllExcelInstances()
        Try
            Dim ExProcs As List(Of Process) = Process.GetProcessesByName("EXCEL").ToList
            For Each xProc In ExProcs
                xProc.Kill()
            Next
        Catch
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        Try
            Kill_It_Softly()       'Attempt to release objects
        Catch
        End Try
        MyBase.Finalize()
    End Sub

    Public Sub New()
        IsReady = False
    End Sub

End Class